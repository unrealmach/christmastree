package com.sl.christmasTree.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sl.christmasTree.model.Cuenta;
import com.sl.christmasTree.model.HistorialTransaccion;
import com.sl.christmasTree.repository.HistorialTransaccionRepository;
import com.sl.christmasTree.rest.ClienteController;

@Service
public class HistorialTransaccionServiceImpl implements HistorialTransaccionService {
	
	private static final Logger logger = LoggerFactory.getLogger(HistorialTransaccionServiceImpl.class);
	
	@Autowired
	private HistorialTransaccionRepository historialTransaccionRepository;
	
	@Autowired
	private CuentaService cuentaService;
	
	@Override
	public List<HistorialTransaccion> verMovimientosCliente(String unique_code_client) {
		Cuenta cuenta = cuentaService.findByUniqueCodeClient(unique_code_client);
		List<HistorialTransaccion> historial =
				historialTransaccionRepository. findByCuenta(cuenta.getId());
		return historial;
	}
	
}
