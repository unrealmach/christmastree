package com.sl.christmasTree.service;

import java.util.List;

import com.sl.christmasTree.model.Cliente;

public interface ClienteService {
	
	boolean addNewClient(Cliente new_cli);
	List<Cliente> getAllClients();
	List<Cliente> findCliente(String findText);
	Cliente getInfoClient(String unique_code);
	
	
}
