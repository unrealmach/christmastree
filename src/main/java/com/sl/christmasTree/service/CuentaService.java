package com.sl.christmasTree.service;

import com.sl.christmasTree.dto.request.AbonarDineroRequestDTO;
import com.sl.christmasTree.dto.request.RetirarDineroRequestDTO;
import com.sl.christmasTree.dto.response.EstadoCuentaResponseDTO;
import com.sl.christmasTree.model.Cliente;
import com.sl.christmasTree.model.Cuenta;

public interface CuentaService {
	
	EstadoCuentaResponseDTO getEstadoCuenta(String unique_code_client);
//	boolean abonarDinero(AbonarDineroRequestDTO request);
	//boolean retirarDinero(RetirarDineroRequestDTO request); 
	Cuenta findByUniqueCodeClient(String unique_code); 
	void createNewNode(Integer christmas_tree_id_in, String code_ref_padre_in, String code_ref_cliente_in, Integer cuenta_cliente_id );
	boolean abonarDinero(AbonarDineroRequestDTO request);
	boolean withdrawals(RetirarDineroRequestDTO request);
	boolean darProductoGratis(String unique_code);
}
