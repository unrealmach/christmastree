package com.sl.christmasTree.service;

import java.util.List;

import com.sl.christmasTree.model.Cliente;
import com.sl.christmasTree.model.HistorialTransaccion;

public interface HistorialTransaccionService {
	List<HistorialTransaccion> verMovimientosCliente(String unique_code_client);
}
