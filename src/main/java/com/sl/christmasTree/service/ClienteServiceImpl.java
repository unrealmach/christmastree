package com.sl.christmasTree.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sl.christmasTree.model.Cliente;
import com.sl.christmasTree.repository.ClienteRepository;
import com.sl.christmasTree.rest.ClienteController;

@Service
public class ClienteServiceImpl implements ClienteService{

	private static final Logger logger = LoggerFactory.getLogger(ClienteServiceImpl.class);
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public List<Cliente> findCliente(String findText) {
		List<Cliente> clienteList = new ArrayList<Cliente>();
		clienteList = clienteRepository.findClienteByUniqueCodeOrDniOrNombres(findText);
		return clienteList;
	}

	@Override
	public boolean addNewClient(Cliente new_cli) {
		return clienteRepository.save(new_cli) != null;
	}

	@Override
	public List<Cliente> getAllClients() {
		return clienteRepository.findAll();
	}
	
	@Override
	public Cliente getInfoClient(String unique_code) {
		return clienteRepository.findByUniqueCode(unique_code);
	}
		
}
