package com.sl.christmasTree.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sl.christmasTree.dto.request.AbonarDineroRequestDTO;
import com.sl.christmasTree.dto.request.RetirarDineroRequestDTO;
import com.sl.christmasTree.dto.response.EstadoCuentaResponseDTO;
import com.sl.christmasTree.model.Cliente;
import com.sl.christmasTree.model.Cuenta;
import com.sl.christmasTree.repository.ClienteRepository;
import com.sl.christmasTree.repository.CuentaRepository;
import com.sl.christmasTree.rest.CuentaController;

@Service
public class CuentaServiceImpl implements CuentaService {
	private static final Logger logger = LoggerFactory.getLogger(CuentaServiceImpl.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CuentaRepository cuentaRepository;

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private ClienteService clienteService;

	@Override
	public EstadoCuentaResponseDTO getEstadoCuenta(String unique_code_client) {
		Cuenta cuenta = findByUniqueCodeClient(unique_code_client);

		EstadoCuentaResponseDTO estadoCuentaResponseDTO = new EstadoCuentaResponseDTO();
		estadoCuentaResponseDTO.setCantidadUltimoMovimieto(0);
		estadoCuentaResponseDTO.setFechaUltimoUso(null);
		estadoCuentaResponseDTO.setMonto(cuenta.getMonto());
		estadoCuentaResponseDTO.setUltimoMovimiento(null);

		return estadoCuentaResponseDTO;
	}

	@Override
	public boolean abonarDinero(AbonarDineroRequestDTO request) {
		StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("add_money_tree");

		storedProcedure.registerStoredProcedureParameter("unique_code_client", String.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("monto_inicial", String.class, ParameterMode.IN);

		storedProcedure.setParameter("unique_code_client", request.getUnique_code_client());
		storedProcedure.setParameter("monto_inicial", request.getAbono());
		logger.debug(request.toString());
		storedProcedure.execute();
		return true;
	}

	@Override
	public boolean withdrawals(RetirarDineroRequestDTO request) {
		StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("withdrawals");

		storedProcedure.registerStoredProcedureParameter("code_ref_cliente_in", String.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("monto_in", String.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("is_valid", Boolean.class, ParameterMode.OUT);

		storedProcedure.setParameter("code_ref_cliente_in", request.getUnique_code_client());
		storedProcedure.setParameter("monto_in", request.getCantidad());

		storedProcedure.execute();
		return (boolean) storedProcedure.getOutputParameterValue("is_valid");
	}

	@Override
	public Cuenta findByUniqueCodeClient(String unique_code) {

		Cuenta cuenta = new Cuenta();
		Cliente cliente = clienteService.getInfoClient(unique_code);
		cuenta = cuentaRepository.findByCliente(cliente);
		return cuenta;
	}

	@Override
	public void createNewNode(Integer christmas_tree_id_in, String code_ref_padre_in, String code_ref_cliente_in,
			Integer cuenta_cliente_id) {
		StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("create_new_node");

		storedProcedure.registerStoredProcedureParameter("christmas_tree_id_in", Integer.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("code_ref_padre_in", String.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("code_ref_cliente_in", String.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("cuenta_cliente_id", Integer.class, ParameterMode.IN);

		storedProcedure.setParameter("christmas_tree_id_in", christmas_tree_id_in);
		storedProcedure.setParameter("code_ref_padre_in", code_ref_padre_in);
		storedProcedure.setParameter("code_ref_cliente_in", code_ref_cliente_in);
		storedProcedure.setParameter("cuenta_cliente_id", cuenta_cliente_id);
		storedProcedure.execute();

	}

	@Override
	public boolean darProductoGratis(String code_ref_cliente_in) {
		StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("do_gift");
		storedProcedure.registerStoredProcedureParameter("code_ref_cliente_in", String.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("is_valid", Boolean.class, ParameterMode.OUT);
		storedProcedure.setParameter("code_ref_cliente_in", code_ref_cliente_in);
		storedProcedure.execute();
		return (boolean) storedProcedure.getOutputParameterValue("is_valid");
	}

}
