package com.sl.christmasTree.dto.response;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sl.christmasTree.dto.BaseDTO;
import com.sl.christmasTree.dto.ResponseBody;
import com.sl.christmasTree.model.Cliente;
import com.sl.christmasTree.model.Cuenta;
import com.sl.christmasTree.model.HistorialTransaccion;
import com.sl.christmasTree.utils.JPADefines.TipoTransaccionEnum;

@JsonInclude(value = Include.NON_NULL)
public class ClienteResponseDTO extends ResponseBody {

	private String nombres;
	private String dni;
	private String celular;
	private String email;
	private String unique_code;

	private Integer cuenta_id;
	private BigDecimal monto;
	private Date fecha_ultimo_uso;
	private String tipo;
	private BigDecimal cantidad_ultima_transaccion;

	public ClienteResponseDTO(Cliente cliente) {
		super();
		this.nombres = cliente.getNombres();
		this.dni = cliente.getDni();
		this.celular = cliente.getCelular();
		this.email = cliente.getEmail();
		this.unique_code = cliente.getUniqueCode();
	}

	public void setDatosCuenta(Cuenta cuenta) {
		this.monto = cuenta.getMonto();
		this.cuenta_id = cuenta.getId();
		this.fecha_ultimo_uso = cuenta.getFechaUltimoUso();
	}

	public void setDatosHistorialTransaccion(HistorialTransaccion ht) {
		this.tipo = ht.getTipo();
		this.cantidad_ultima_transaccion = ht.getCantidad();
	}

	public ClienteResponseDTO() {
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUnique_code() {
		return unique_code;
	}

	public void setUnique_code(String unique_code) {
		this.unique_code = unique_code;
	}

	public Integer getCuenta_id() {
		return cuenta_id;
	}

	public void setCuenta_id(Integer cuenta_id) {
		this.cuenta_id = cuenta_id;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public Date getFecha_ultimo_uso() {
		return fecha_ultimo_uso;
	}

	public void setFecha_ultimo_uso(Date fecha_ultimo_uso) {
		this.fecha_ultimo_uso = fecha_ultimo_uso;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public BigDecimal getCantidad_ultima_transaccion() {
		return cantidad_ultima_transaccion;
	}

	public void setCantidad_ultima_transaccion(BigDecimal cantidad_ultima_transaccion) {
		this.cantidad_ultima_transaccion = cantidad_ultima_transaccion;
	}

}
