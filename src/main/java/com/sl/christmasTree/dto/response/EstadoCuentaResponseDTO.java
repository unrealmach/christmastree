package com.sl.christmasTree.dto.response;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sl.christmasTree.dto.BaseDTO;
import com.sl.christmasTree.dto.ResponseBody;
import com.sl.christmasTree.utils.JPADefines.TipoTransaccionEnum;

@JsonInclude(value=Include.NON_NULL)
public class EstadoCuentaResponseDTO extends ResponseBody {
	private BigDecimal monto;
	private Date fechaUltimoUso;
	private double cantidadUltimoMovimiento;
	private TipoTransaccionEnum ultimoMovimiento;
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal bigDecimal) {
		this.monto = bigDecimal;
	}
	public Date getFechaUltimoUso() {
		return fechaUltimoUso;
	}
	public void setFechaUltimoUso(Date fechaUltimoUso) {
		this.fechaUltimoUso = fechaUltimoUso;
	}
	public double getCantidadUltimoMovimieto() {
		return cantidadUltimoMovimiento;
	}
	public void setCantidadUltimoMovimieto(double cantidadUltimoMovimieto) {
		this.cantidadUltimoMovimiento = cantidadUltimoMovimieto;
	}
	public TipoTransaccionEnum getUltimoMovimiento() {
		return ultimoMovimiento;
	}
	public void setUltimoMovimiento(TipoTransaccionEnum ultimoMovimiento) {
		this.ultimoMovimiento = ultimoMovimiento;
	}
	
	
}
