package com.sl.christmasTree.dto.response;

import java.util.ArrayList;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sl.christmasTree.dto.BaseDTO;
import com.sl.christmasTree.dto.ResponseBody;
import com.sl.christmasTree.model.Cliente;
import com.sl.christmasTree.model.Cuenta;
import com.sl.christmasTree.model.HistorialTransaccion;
import com.sl.christmasTree.utils.JPADefines.TipoTransaccionEnum;

@JsonInclude(value = Include.NON_NULL)
public class SimpleResponseDTO extends ResponseBody {

	
	public SimpleResponseDTO() {
		super();
	}


}
