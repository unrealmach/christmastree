package com.sl.christmasTree.dto.request;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sl.christmasTree.dto.BaseDTO;
import com.sl.christmasTree.utils.JPADefines.TipoTransaccionEnum;

@JsonInclude(value=Include.NON_NULL)
public  class NuevoClienteRequestDTO {
	private String nombres;
	private String dni;
	private String celular;
	private String email;
	private String code_ref_padre_in;
	private Integer christmas_tree_id_in;
	
	public NuevoClienteRequestDTO() {	}
	
	@JsonCreator
	public NuevoClienteRequestDTO(@JsonProperty("nombres") String nombres,
			@JsonProperty("dni")  String dni,
			@JsonProperty("celular") String celular,
			@JsonProperty("email") String email,
			@JsonProperty("code_ref_padre_in") String code_ref_padre_in,
			@JsonProperty("christmas_tree_id_in") Integer christmas_tree_id_in) {
		super();
		this.nombres = nombres;
		this.dni = dni;
		this.celular = celular;
		this.email = email;
		this.code_ref_padre_in=code_ref_padre_in;
		this.christmas_tree_id_in=christmas_tree_id_in;
	}
	
	public String getCode_ref_padre_in() {
		return code_ref_padre_in;
	}

	public void setCode_ref_padre_in(String code_ref_padre_in) {
		this.code_ref_padre_in = code_ref_padre_in;
	}

	public Integer getChristmas_tree_id_in() {
		return christmas_tree_id_in;
	}

	public void setChristmas_tree_id_in(Integer christmas_tree_id_in) {
		this.christmas_tree_id_in = christmas_tree_id_in;
	}

	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	

	
	
}
