package com.sl.christmasTree.dto.request;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sl.christmasTree.dto.BaseDTO;
import com.sl.christmasTree.utils.JPADefines.TipoTransaccionEnum;

@JsonInclude(value=Include.NON_NULL)
public class AbonarDineroRequestDTO  extends BaseDTO{
	private String cantidad;
	private String unique_code_client;
	private String abono;
	
	
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getUnique_code_client() {
		return unique_code_client;
	}
	public void setUnique_code_client(String unique_code_client) {
		this.unique_code_client = unique_code_client;
	}
	public String getAbono() {
		return abono;
	}
	public void setAbono(String abono) {
		this.abono = abono;
	}

	
	
}
