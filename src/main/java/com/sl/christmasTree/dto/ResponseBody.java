package com.sl.christmasTree.dto;

public abstract class ResponseBody {

	protected Integer code;
	protected String message;
	
	public ResponseBody(){
		
	}
	
	public ResponseBody(Integer code, String message) {
		super();
		this.code = code;
		this.message = message;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
