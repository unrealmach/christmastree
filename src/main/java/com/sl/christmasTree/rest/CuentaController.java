package com.sl.christmasTree.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.sl.christmasTree.dto.BaseDTO;
import com.sl.christmasTree.dto.JsonViewsDTO;
import com.sl.christmasTree.dto.request.AbonarDineroRequestDTO;
import com.sl.christmasTree.dto.request.NuevoClienteRequestDTO;
import com.sl.christmasTree.dto.request.RetirarDineroRequestDTO;
import com.sl.christmasTree.dto.response.AbonarArbolResponseDTO;
import com.sl.christmasTree.dto.response.ClienteResponseDTO;
import com.sl.christmasTree.dto.response.RetiroArbolResponseDTO;
import com.sl.christmasTree.dto.response.SimpleResponseDTO;
import com.sl.christmasTree.model.Cliente;
import com.sl.christmasTree.model.Cuenta;
import com.sl.christmasTree.model.HistorialTransaccion;
import com.sl.christmasTree.service.ClienteService;
import com.sl.christmasTree.service.CuentaService;
import com.sl.christmasTree.service.HistorialTransaccionService;
import com.sl.christmasTree.utils.ClienteUtil;



@RestController
@RequestMapping(value = "/api/cuenta")
public class CuentaController {
	private static final Logger logger = LoggerFactory.getLogger(CuentaController.class);
		
	@Autowired
	private CuentaService cuentaService;
	
	@Autowired
	private HistorialTransaccionService hTService;
			
	@PostMapping("/abonarArbol")
	public @ResponseBody AbonarArbolResponseDTO abonarArbol(@Valid @RequestBody AbonarDineroRequestDTO request) {
		
		Boolean flag = false;
		AbonarArbolResponseDTO ret = new AbonarArbolResponseDTO();
		try {
			flag = cuentaService.abonarDinero(request);
			ret.setCode(0);
			ret.setSuccess(flag);
		} catch (Exception e) {
			ret.setCode(1);
			ret.setMessage(e.getMessage());
			ret.setSuccess(flag);
		}
		return ret;
	} 
	
	@PostMapping("/withdrawals")
	public @ResponseBody RetiroArbolResponseDTO withdrawals(@Valid @RequestBody RetirarDineroRequestDTO request) {
		
		Boolean flag = false;
		RetiroArbolResponseDTO ret = new RetiroArbolResponseDTO() ;
		
		try {
			flag = cuentaService.withdrawals(request);
			System.out.println(flag);
			if(flag) {
				ret.setCode(0);
			}else {
				ret.setCode(1);
				ret.setMessage("La cuenta no tiene fondos suficientes o no tiene referidos válidos");
			}
			
		} catch (Exception e) {
			ret.setCode(1);
			ret.setMessage(e.getMessage());
			
		}
		return ret;
	} 
	
	
	
	
	@GetMapping("/getMovimientosCliente/{unique_code_client}")
	@JsonView(JsonViewsDTO.HistorialTransaccionesView.class)
	public @ResponseBody List<HistorialTransaccion> getMovimientosCliente(@PathVariable String unique_code_client){
		List<HistorialTransaccion> ret = new ArrayList<HistorialTransaccion>() ;
		try {
			ret = hTService.verMovimientosCliente(unique_code_client);
		} catch (Exception e) {
			logger.debug(e.getMessage());
			ret = null;
		}
		return ret;
	}
	
	@GetMapping("/doGift/{unique_code_client}")
	public @ResponseBody SimpleResponseDTO doGift(@PathVariable String unique_code_client){
		
		SimpleResponseDTO ret = new SimpleResponseDTO();
		ret.setCode(0);
		ret.setMessage(String.valueOf(cuentaService.darProductoGratis(unique_code_client)));
		
		return ret;
	}
	
}
