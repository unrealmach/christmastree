package com.sl.christmasTree.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sl.christmasTree.dto.request.NuevoClienteRequestDTO;
import com.sl.christmasTree.dto.response.ClienteResponseDTO;
import com.sl.christmasTree.model.Cliente;
import com.sl.christmasTree.model.Cuenta;
import com.sl.christmasTree.model.HistorialTransaccion;
import com.sl.christmasTree.service.ClienteService;
import com.sl.christmasTree.service.CuentaService;
import com.sl.christmasTree.service.HistorialTransaccionService;
import com.sl.christmasTree.utils.ClienteUtil;

@RestController
@RequestMapping(value = "/api/client")
public class ClienteController {
	private static final Logger logger = LoggerFactory.getLogger(ClienteController.class);
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private CuentaService cuentaService;
	
	@Autowired
	private HistorialTransaccionService htService;
	
	
	@Autowired
	private ClienteUtil clienteUtil;
	

	@GetMapping("/getInfoClient/{unique_code}")
	public @ResponseBody List<ClienteResponseDTO> getInfoClient(@PathVariable String unique_code) {
		List<ClienteResponseDTO> retList = new  ArrayList<ClienteResponseDTO>();
		try {
			List<Cliente> clienteList = clienteService.findCliente(unique_code);
			System.out.println(clienteList.size()+"");
			clienteList.stream().forEach(
					(cliente)->{
						ClienteResponseDTO ret = new ClienteResponseDTO();
						ret = new ClienteResponseDTO(cliente);
						System.out.println(cliente.getUniqueCode());
						Cuenta cuenta = cuentaService.findByUniqueCodeClient(cliente.getUniqueCode());
						System.out.println(cuenta.getId());
						HistorialTransaccion ht = 
								htService.verMovimientosCliente(cliente.getUniqueCode())
								.get(htService.verMovimientosCliente(cliente.getUniqueCode()).size()-1);
						ret.setDatosCuenta(cuenta);
						ret.setDatosHistorialTransaccion(ht);
						
						retList.add(ret);
					});
			
				return retList;
		} catch (Exception e) {
			ClienteResponseDTO ret = new ClienteResponseDTO();
			ret.setCode(1);
			ret.setMessage(e.getMessage());
			retList.add(ret);
		}
		
		return retList;
	}
	
	@PostMapping("/addNewClient")
	public @ResponseBody ClienteResponseDTO addNewClient(@Valid @RequestBody NuevoClienteRequestDTO request) {
		ClienteResponseDTO ret = new ClienteResponseDTO();
		try {
			Cliente cliente = new Cliente(request);
			cliente.setUniqueCode(clienteUtil.getUniqueCode(cliente));
			boolean flag = clienteService.addNewClient(cliente);
			ret = new ClienteResponseDTO(cliente);
			if(!flag) {
				ret.setCode(1);
			}else {
				Cuenta cuentaCli = cuentaService.findByUniqueCodeClient(cliente.getUniqueCode());
				
				if(request.getCode_ref_padre_in().trim().equals("")) {
					request.setCode_ref_padre_in("MAUR1004");
				}
				
				cuentaService
				.createNewNode(request.getChristmas_tree_id_in(),
						request.getCode_ref_padre_in(), cliente.getUniqueCode(),cuentaCli.getId());
				ret.setCode(0);
			}
		} catch (Exception e) {
			ret.setCode(1);
			ret.setMessage(e.getMessage());
		}
		
		return ret;
	} 

	@GetMapping("/getAllClients")
	public @ResponseBody List<Cliente> getAllClients(){

		
		return clienteService.getAllClients();
	}
	
	

	
}
