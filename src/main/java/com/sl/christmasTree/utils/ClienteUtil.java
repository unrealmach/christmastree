package com.sl.christmasTree.utils;

import org.springframework.stereotype.Component;

import com.sl.christmasTree.model.Cliente;

@Component
public class ClienteUtil {

	public String getUniqueCode(Cliente cliente) {
		String code = cliente.getNombres().substring(0, 4)+ cliente.getDni().substring(0, 4);
		return code;
	}
}
