package com.sl.christmasTree.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sl.christmasTree.model.Cliente;


@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

	Cliente findByUniqueCode(String uniqueCode); 
	
	
	@Query(value="SELECT c FROM Cliente c "
			+ "WHERE c.dni LIKE %:textfind% "
			+ "OR c.uniqueCode LIKE %:textfind% "
			+ "OR c.nombres LIKE %:textfind% ")
    public List<Cliente> findClienteByUniqueCodeOrDniOrNombres(@Param("textfind") String findText);
}
