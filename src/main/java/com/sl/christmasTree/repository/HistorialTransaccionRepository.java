package com.sl.christmasTree.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sl.christmasTree.model.Cliente;
import com.sl.christmasTree.model.Cuenta;
import com.sl.christmasTree.model.HistorialTransaccion;

public interface HistorialTransaccionRepository extends JpaRepository<HistorialTransaccion, Integer> {

	@Query("SELECT h FROM HistorialTransaccion h, Cuenta c WHERE "
			+ "h.cuenta.id = c.id AND h.cuenta.id = :cuentaId")
	List<HistorialTransaccion> findByCuenta(@Param("cuentaId") int cuentaId);
	
	
}
 