package com.sl.christmasTree.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import com.sl.christmasTree.model.Cliente;
import com.sl.christmasTree.model.Cuenta;

public interface CuentaRepository extends JpaRepository<Cuenta, Integer> {

	Cuenta findByCliente(Cliente cliente);
	

}
