package com.sl.christmasTree.cron;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class ScheduledTask {

	private static final Logger logger = LoggerFactory.getLogger(ScheduledTask.class);
    
	@PersistenceContext
	private EntityManager em;
	
	// a la 1 de la mañana cada dia
    @Scheduled(cron = "0 0 1 * * *")
    public boolean autorizarSeteoCero() {
    	Boolean flag = false;
    	StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("set_cero_all_points");
		storedProcedure.execute();
		flag = (boolean) storedProcedure.getOutputParameterValue("is_valid");
		logger.debug("EJECUCÍON CRON SETEO A 0 PUNTOS TODOS LOS NODOS: "+ String.valueOf(flag) );
		return flag;
    }
}
