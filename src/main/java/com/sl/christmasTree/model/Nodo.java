package com.sl.christmasTree.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@NamedQuery(name="Nodo.findAll", query="SELECT n FROM Nodo n")
public class Nodo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;

	@Column(name="code_ref_padre")
	private String codeRefPadre;

	@Column(name="nivel")
	private Integer nivel;
	
	@Column(name="points")
	private Integer points;
	
	@Column(name="number_children")
	private Integer numberChildren;

	@Column(name="was_used_promo")
	private Boolean wasUsedPromo;

	//bi-directional many-to-one association to HistoricPromo
	@OneToMany(mappedBy="nodo")
	private List<HistoricPromo> historicPromos;

	//bi-directional many-to-one association to ChristmasTree
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="christmas_tree_id")
	private ChristmasTree christmasTree;

	//bi-directional many-to-one association to Cliente
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="code_ref_cliente", referencedColumnName="unique_code")
	private Cliente cliente;

	public Nodo() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodeRefPadre() {
		return this.codeRefPadre;
	}

	public void setCodeRefPadre(String codeRefPadre) {
		this.codeRefPadre = codeRefPadre;
	}

	public Integer getNivel() {
		return this.nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public Integer getPoints() {
		return this.points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Boolean getWasUsedPromo() {
		return this.wasUsedPromo;
	}

	public void setWasUsedPromo(Boolean wasUsedPromo) {
		this.wasUsedPromo = wasUsedPromo;
	}

	public List<HistoricPromo> getHistoricPromos() {
		return this.historicPromos;
	}

	public void setHistoricPromos(List<HistoricPromo> historicPromos) {
		this.historicPromos = historicPromos;
	}

	public HistoricPromo addHistoricPromo(HistoricPromo historicPromo) {
		getHistoricPromos().add(historicPromo);
		historicPromo.setNodo(this);

		return historicPromo;
	}

	public HistoricPromo removeHistoricPromo(HistoricPromo historicPromo) {
		getHistoricPromos().remove(historicPromo);
		historicPromo.setNodo(null);

		return historicPromo;
	}

	public ChristmasTree getChristmasTree() {
		return this.christmasTree;
	}

	public void setChristmasTree(ChristmasTree christmasTree) {
		this.christmasTree = christmasTree;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Integer getNumberChildren() {
		return numberChildren;
	}

	public void setNumberChildren(Integer numberChildren) {
		this.numberChildren = numberChildren;
	}

}
