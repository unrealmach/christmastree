package com.sl.christmasTree.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreType;

@Entity
@NamedQuery(name="Cuenta.findAll", query="SELECT c FROM Cuenta c")
public class Cuenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "fecha_creacion")
	private Timestamp fechaCreacion;

	@Column(name = "fecha_ultimo_uso")
	private Timestamp fechaUltimoUso;

	@Column(name = "monto", columnDefinition = "NUMERIC(27,20)")
	private BigDecimal monto;

	//bi-directional many-to-one association to Cliente
	@JsonIgnoreProperties(value="cuentas")
	@ManyToOne
	private Cliente cliente;

	//bi-directional many-to-one association to HistorialTransaccion
	@JsonIgnoreProperties(value="cuenta")
	@OneToMany(mappedBy="cuenta")
	private List<HistorialTransaccion> historialTransaccions;

	public Cuenta() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Timestamp getFechaUltimoUso() {
		return this.fechaUltimoUso;
	}

	public void setFechaUltimoUso(Timestamp fechaUltimoUso) {
		this.fechaUltimoUso = fechaUltimoUso;
	}

	public BigDecimal getMonto() {
		return this.monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<HistorialTransaccion> getHistorialTransaccions() {
		return this.historialTransaccions;
	}

	public void setHistorialTransaccions(List<HistorialTransaccion> historialTransaccions) {
		this.historialTransaccions = historialTransaccions;
	}

	public HistorialTransaccion addHistorialTransaccion(HistorialTransaccion historialTransaccion) {
		getHistorialTransaccions().add(historialTransaccion);
		historialTransaccion.setCuenta(this);

		return historialTransaccion;
	}

	public HistorialTransaccion removeHistorialTransaccion(HistorialTransaccion historialTransaccion) {
		getHistorialTransaccions().remove(historialTransaccion);
		historialTransaccion.setCuenta(null);

		return historialTransaccion;
	}

}
