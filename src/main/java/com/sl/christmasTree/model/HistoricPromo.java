package com.sl.christmasTree.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="historic_promo")
@NamedQuery(name="HistoricPromo.findAll", query="SELECT h FROM HistoricPromo h")
public class HistoricPromo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;

	@Column(name = "date")
	private Timestamp date;

	@Column(name = "description")
	private String description;

	//bi-directional many-to-one association to Nodo
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="node")
	private Nodo nodo;

	public HistoricPromo() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Nodo getNodo() {
		return this.nodo;
	}

	public void setNodo(Nodo nodo) {
		this.nodo = nodo;
	}

}
