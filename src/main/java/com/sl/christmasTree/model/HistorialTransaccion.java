package com.sl.christmasTree.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import com.sl.christmasTree.dto.JsonViewsDTO;
import com.sl.christmasTree.utils.JPADefines.TipoTransaccionEnum;

@Entity
@Table(name="historial_transaccion")
@NamedQuery(name="HistorialTransaccion.findAll", query="SELECT h FROM HistorialTransaccion h")
public class HistorialTransaccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	@JsonView(JsonViewsDTO.HistorialTransaccionesView.class)
	private Integer id;

	@JsonView(JsonViewsDTO.HistorialTransaccionesView.class)
	@Column(name="cantidad",columnDefinition = "NUMERIC(27,20)")
	private BigDecimal cantidad;

	@JsonView(JsonViewsDTO.HistorialTransaccionesView.class)
	@Column(name = "date")
	private Timestamp date;

	@JsonView(JsonViewsDTO.HistorialTransaccionesView.class)
	@Column(name="monto_actual",columnDefinition = "NUMERIC(27,20)")
	private BigDecimal montoActual;

	@JsonView(JsonViewsDTO.HistorialTransaccionesView.class)
	@Column(name="monto_anterior",columnDefinition = "NUMERIC(27,20)")
	private BigDecimal montoAnterior;
	
	@JsonView(JsonViewsDTO.HistorialTransaccionesView.class)
	@Column(name="tipo",columnDefinition = "tipo_transaccion")
	private String tipo;

	//bi-directional many-to-one association to Cuenta
	@JsonIgnoreProperties(value= {"historialTransaccions","cliente"})
	@ManyToOne()
	private Cuenta cuenta;

	public HistorialTransaccion() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public BigDecimal getMontoActual() {
		return this.montoActual;
	}

	public void setMontoActual(BigDecimal montoActual) {
		this.montoActual = montoActual;
	}

	public BigDecimal getMontoAnterior() {
		return this.montoAnterior;
	}

	public void setMontoAnterior(BigDecimal montoAnterior) {
		this.montoAnterior = montoAnterior;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Cuenta getCuenta() {
		return this.cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

}
