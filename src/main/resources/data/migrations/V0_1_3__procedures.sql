 DROP  FUNCTION IF EXISTS find_parents(character varying) CASCADE;

CREATE OR REPLACE FUNCTION find_parents(code_ref_cliente_in VARCHAR) 
RETURNS TABLE ( lvl INTEGER, padre  VARCHAR)
AS $body$
BEGIN
return QUERY
WITH RECURSIVE nodos_padres AS (

	SELECT  nivel, code_ref_padre, code_ref_cliente
		FROM nodo where code_ref_cliente = code_ref_cliente_in

	UNION
		SELECT 
		n.nivel, n.code_ref_padre, n.code_ref_cliente
		FROM nodo n INNER JOIN nodos_padres np 
		ON n.code_ref_cliente=np.code_ref_padre
				)  
 SELECT nivel, code_ref_padre from nodos_padres;
END $body$ LANGUAGE 'plpgsql';

-- SELECT * from find_parents('moco6003')

DROP  FUNCTION IF EXISTS calculate_money_by_lvl(varchar, numeric) CASCADE;

CREATE OR REPLACE FUNCTION calculate_money_by_lvl(unque_code_client varchar,monto_inicial numeric(27,20)) 
RETURNS numeric(27,20)[]
AS $body$
DECLARE
max_lvl integer := 0;
money_by_lvl numeric(27,20)[];
temp_money numeric(27,20) :=0;
iterator integer := 0;
BEGIN

temp_money := monto_inicial;
SELECT count(*) -1 from find_parents(unque_code_client) INTO max_lvl;
WHILE iterator < max_lvl
   LOOP
	   temp_money := temp_money/2;
	   money_by_lvl := money_by_lvl || temp_money + 0.00;
	   iterator := iterator + 1;
   END LOOP;
RETURN money_by_lvl;
END $body$ LANGUAGE 'plpgsql';


-- select calculate_money_by_lvl(10.0);


 -- SELECT * from find_parents('moco6003') where lvl = 5  ;
-- SELECT pg_typeof( calculate_money_by_lvl(10.0));
--Select cu.* from cliente cl INNER JOIN  cuenta cu ON cu.cliente_id = cl.id where cl.unique_code = 'Aman1030'

DROP  FUNCTION IF EXISTS add_money_tree( varchar, varchar) CASCADE;



CREATE OR REPLACE FUNCTION add_money_tree(unique_code_client varchar, monto_inicial varchar) 
RETURNS boolean
AS $body$
DECLARE
size_array integer := 0; -- tamaño del array de abonos, es igual al maximo nivel del nodo hijo
money_by_lvl numeric[]; -- array que tiene los abonos para los padres de un nodo
-- temp_money numeric :=0;
iterator integer := 0; -- ayuda a conocer a que nodo padre abonar 
temp_nodo_padre_row record; -- record de nodo padre
temp_cuenta_padre_row record; -- record de cuent de un nodo padre
temp_client_row record; -- record temporal para almacenar el cliente que desea abonar al arbol
total_anterior_in numeric;
total_actual_in numeric;
temp_cuenta_client_row record;

temp_puntos integer :=0;

BEGIN
--agregar el cliente XD
select calculate_money_by_lvl(unique_code_client,monto_inicial::numeric)  INTO money_by_lvl;
select array_length( money_by_lvl ,1) INTO size_array;

IF money_by_lvl IS NOT NULL THEN
 FOR i IN array_lower(money_by_lvl, 1) .. array_upper(money_by_lvl, 1)
   LOOP
    iterator := size_array -i +1;
	SELECT * INTO temp_nodo_padre_row from find_parents(unique_code_client) where lvl = iterator ;
	
	 IF temp_nodo_padre_row IS NULL THEN
	 	CONTINUE;
	 END IF;
	 
	Select cu.* INTO temp_cuenta_padre_row from cliente cl 
	INNER JOIN  cuenta cu ON cu.cliente_id = cl.id 
		where cl.unique_code = temp_nodo_padre_row.padre; 
	SELECT temp_cuenta_padre_row.monto INTO total_anterior_in;
	SELECT total_anterior_in + money_by_lvl[i] INTO total_actual_in;
	UPDATE cuenta ct set monto = total_actual_in, fecha_ultimo_uso = now()::TIMESTAMP where ct.id=temp_cuenta_padre_row.id;
	
		RAISE NOTICE '%', total_anterior_in;
		
		
	PERFORM create_transaccions_update_cuenta(temp_cuenta_padre_row.id,
				money_by_lvl[i]::text,total_actual_in::text,total_anterior_in::text,'DEPOSITO');

   END LOOP;
  END IF; 
   -- actualiza la cuenta del cliente con el valor sobrante de su abono al arbol
   SELECT * FROM cliente where unique_code = unique_code_client INTO temp_client_row ;
   SELECT monto FROM cuenta where cliente_id = temp_client_row.id INTO total_anterior_in;
   SELECT total_anterior_in + money_by_lvl[size_array] INTO total_actual_in;
   --UPDATE cuenta ct set monto = monto + monto_inicial::numeric/2, fecha_ultimo_uso = now()::TIMESTAMP  where ct.cliente_id=temp_client_row.id;
   UPDATE cuenta ct set monto = total_actual_in, fecha_ultimo_uso = now()::TIMESTAMP  where ct.cliente_id=temp_client_row.id;
   
    SELECT * from cuenta ct where ct.cliente_id = temp_client_row.id INTO temp_cuenta_client_row;
   PERFORM create_transaccions_update_cuenta(temp_cuenta_client_row.id,money_by_lvl[size_array]::text,
						total_actual_in::text,total_anterior_in::text,'DEPOSITO');

 -- Calculo y actualización de puntos del nodo cliente
    SELECT points FROM nodo WHERE code_ref_cliente = unique_code_client INTO temp_puntos;
    
   UPDATE nodo SET points = temp_puntos + trunc(monto_inicial::numeric) WHERE code_ref_cliente = unique_code_client ;
RETURN true;
END $body$ LANGUAGE 'plpgsql';


--select add_money_tree('Aman8679','500.0');
