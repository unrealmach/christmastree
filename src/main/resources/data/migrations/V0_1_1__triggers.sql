DROP  FUNCTION IF EXISTS create_account_before_insert_client() CASCADE;

CREATE OR REPLACE FUNCTION create_account_after_insert_client()
RETURNS trigger AS '
BEGIN
INSERT INTO cuenta (fecha_creacion,fecha_ultimo_uso,cliente_id) values (now()::TIMESTAMP,now()::TIMESTAMP,NEW.id);
   RETURN NEW;
END' LANGUAGE 'plpgsql';

CREATE TRIGGER create_account_after_insert_client
AFTER INSERT ON cliente
FOR EACH ROW
EXECUTE PROCEDURE create_account_after_insert_client();

