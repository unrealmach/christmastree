-- agrega columnas para confirmar que dicho nodo solo sea usado una vez en cada promocion
-- y una columna para contabilizar los puntos
ALTER TABLE nodo ADD COLUMN was_used_promo boolean default false;
ALTER TABLE nodo ADD COLUMN points integer default 0;
ALTER TABLE nodo ADD COLUMN number_children integer default 0;

ALTER TABLE historial_transaccion ADD COLUMN date timestamp default now();
