-- SELECT validate_gift_from_references_node('COKO5232');


DROP  FUNCTION IF EXISTS validate_gift_from_references_node(character varying,boolean) CASCADE;

CREATE OR REPLACE FUNCTION validate_gift_from_references_node( IN code_ref_cliente_in VARCHAR, OUT is_valid boolean) 
	RETURNS boolean
	AS $body$
DECLARE
	valid_retiro numeric;
	count_valid_lvl_1 numeric :=0;
	count_valid_lvl_2 numeric :=0;
	temprow record;
BEGIN
	is_valid := FALSE;
	-- RAISE NOTICE 'resta %',valid_retiro;
	SELECT count(*)  FROM nodo WHERE code_ref_padre =code_ref_cliente_in AND was_used_promo = FALSE LIMIT 10 INTO count_valid_lvl_1;
	IF count_valid_lvl_1 >= 10
		THEN
		is_valid := TRUE;
	END IF;

	IF is_valid = TRUE
		THEN
			FOR temprow IN SELECT * FROM nodo WHERE code_ref_padre =code_ref_cliente_in AND was_used_promo = FALSE LIMIT 10
			LOOP
				SELECT count(*) FROM nodo WHERE code_ref_padre = temprow.code_ref_cliente INTO count_valid_lvl_2;
				IF count_valid_lvl_2 = 0
					THEN
					is_valid:= is_valid AND FALSE;
				END IF;
				
			END LOOP;
	END IF;

		
END $body$ LANGUAGE 'plpgsql';

DROP  FUNCTION IF EXISTS do_gift(character varying,boolean) CASCADE;

CREATE OR REPLACE FUNCTION do_gift( IN code_ref_cliente_in VARCHAR, OUT is_valid boolean) 
	RETURNS boolean
	AS $body$
DECLARE
	valid_retiro numeric;
	count_valid_lvl_1 numeric :=0;
	count_valid_lvl_2 numeric :=0;
	
	temprow record;
BEGIN
	is_valid := (SELECT validate_gift_from_references_node(code_ref_cliente_in));

	IF is_valid = TRUE
		THEN
			FOR temprow IN SELECT * FROM nodo WHERE code_ref_padre =code_ref_cliente_in AND was_used_promo = FALSE LIMIT 10
			LOOP
				UPDATE nodo SET was_used_promo = TRUE WHERE code_ref_padre = temprow.code_ref_padre;
			END LOOP;
	END IF;

	SELECT * FROM nodo WHERE code_ref_cliente=code_ref_cliente_in INTO temprow;
	INSERT INTO historic_promo (node,description) values (temprow.id,'COBRO PRODUCTO GRATIS'::text);	
END $body$ LANGUAGE 'plpgsql';
