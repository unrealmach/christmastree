-- se agrega el proceso para contar los hijos que se añaden segun la referencia del padre
-- se agrega el proceso para contar los hijos que se añaden segun la referencia del padre
DROP  FUNCTION IF EXISTS create_new_node(integer,character varying,character varying,integer) CASCADE;

CREATE OR REPLACE FUNCTION create_new_node(christmas_tree_id_in integer,code_ref_padre_in varchar, code_ref_cliente_in varchar,cuenta_cliente_id integer) 
RETURNS boolean AS $body$
DECLARE
lvlPadre INTEGER := 0;
number_childrenTmp INTEGER :=0;

BEGIN

SELECT nivel FROM nodo where code_ref_cliente = code_ref_padre_in INTO lvlPadre;
SELECT number_children FROM nodo where code_ref_cliente = code_ref_padre_in INTO number_childrenTmp;

IF lvlPadre IS NULL
THEN
lvlPadre := 0;
ELSE
lvlPadre := lvlPadre + 1;
END IF;
INSERT into nodo (nivel,code_ref_padre,code_ref_cliente,christmas_tree_id) values (lvlPadre,code_ref_padre_in,code_ref_cliente_in,christmas_tree_id_in);
INSERT into historial_transaccion (tipo,cantidad,monto_anterior,monto_actual,cuenta_id) values ('DEPOSITO',0,0,0,cuenta_cliente_id);

UPDATE nodo set number_children = number_childrenTmp +1 where code_ref_cliente = code_ref_padre_in;

return TRUE;
END $body$ LANGUAGE 'plpgsql';

-- select create_new_node(2,'Aman1003','Maur1003');