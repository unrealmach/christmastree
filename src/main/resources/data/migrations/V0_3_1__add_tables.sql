-- agrega tablas para los nuevos cambios y reportes en el sistema
CREATE TABLE historic_promo
(	id SERIAL primary key,
	node integer REFERENCES nodo(id) ON DELETE CASCADE,
	date timestamp default now(),
	description text not null
	
);

CREATE TABLE parameters
(	id serial primary key,
	name varchar(100) not null,
	value text not null,
	date_update timestamp default now() 
	
)
