
DROP  FUNCTION IF EXISTS withdrawals(character varying,numeric) CASCADE;

CREATE OR REPLACE FUNCTION withdrawals( IN code_ref_cliente_in VARCHAR, IN monto_in varchar, OUT is_valid boolean) 
RETURNS boolean
AS $body$
DECLARE
cliente_temp record;
valid_retiro numeric;
cuenta_temp record;
total_anterior numeric :=0;
cantidad_referidos numeric :=0;

BEGIN
SELECT * FROM cliente where unique_code = code_ref_cliente_in INTO cliente_temp;
SELECT monto FROM cuenta where cliente_id = cliente_temp.id INTO total_anterior;
SELECT total_anterior - monto_in::numeric  INTO valid_retiro;

	IF valid_retiro IS NULL
		THEN
		is_valid:=FALSE;
	END IF;

RAISE NOTICE 'resta %',valid_retiro;

select count(*) from nodo  where code_ref_padre = code_ref_cliente_in INTO cantidad_referidos;
		
	IF
	cantidad_referidos>=3  AND valid_retiro > 0	
	THEN
	--cuenta_id_in integer,monto_in varchar, total_actual_in varchar,total_anterior_in varchar, tipo_in varchar
		UPDATE cuenta set monto = valid_retiro, fecha_ultimo_uso = now()::timestamp where cliente_id = cliente_temp.id;
		SELECT * FROM cuenta WHERE cliente_id = cliente_temp.id INTO cuenta_temp;
		PERFORM create_transaccions_update_cuenta(cuenta_temp.id,monto_in::text,valid_retiro::text,total_anterior::text  ,'RETIRO');

		is_valid:=TRUE;

	ELSE
		is_valid:=FALSE;
		
	END IF;

		
END $body$ LANGUAGE 'plpgsql';


DROP  FUNCTION IF EXISTS create_transaccions_update_cuenta(integer,varchar,varchar,varchar,varchar) CASCADE;

CREATE OR REPLACE FUNCTION create_transaccions_update_cuenta(cuenta_id_in integer,monto_in varchar, total_actual_in varchar,total_anterior_in varchar, tipo_in varchar) -- 0.99, retiro
RETURNS boolean AS $BODY$
BEGIN
INSERT INTO historial_transaccion (tipo,cantidad,monto_anterior,monto_actual,cuenta_id)
	values (tipo_in::tipo_transaccion,monto_in::numeric,total_anterior_in::numeric,total_actual_in::numeric,cuenta_id_in );

   RETURN true;
END $BODY$ LANGUAGE 'plpgsql';

