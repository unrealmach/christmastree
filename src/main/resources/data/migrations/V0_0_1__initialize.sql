DROP TABLE IF EXISTS cliente CASCADE ;
DROP TABLE IF EXISTS cuenta CASCADE;
DROP TYPE IF EXISTS tipo_transaccion CASCADE;
DROP TABLE IF EXISTS historial_transaccion CASCADE;
DROP TABLE IF EXISTS nodo CASCADE;
DROP TABLE IF EXISTS christmas_tree;

CREATE TABLE cliente (
	id SERIAL primary key,
	nombres varchar not null,
	dni varchar unique not null,
	celular varchar not null,
	email varchar not null,
	unique_code varchar unique not null
);

CREATE TABLE cuenta(
	id SERIAL primary key,
	monto numeric(27,20) default 0 not null,
	fecha_creacion timestamp default now() not null,
	fecha_ultimo_uso timestamp not null,
	cliente_id integer REFERENCES cliente(id) ON DELETE CASCADE
);

CREATE TYPE tipo_transaccion AS ENUM (
  'DEPOSITO', 
  'RETIRO');

CREATE TABLE historial_transaccion(
	id SERIAL primary key,
	tipo tipo_transaccion,
	cantidad numeric(27,20) not null,
	monto_anterior numeric(27,20) not null,
	monto_actual numeric(27,20) not null,
	cuenta_id integer REFERENCES cuenta(id) ON DELETE CASCADE
);

CREATE TABLE christmas_tree(
	id SERIAL primary key,
	descripcion varchar not null
);

CREATE TABLE nodo (
	id SERIAL primary key,
	nivel integer default 0 not null,
	code_ref_padre varchar not null,
	code_ref_cliente varchar REFERENCES cliente(unique_code) ON DELETE CASCADE,
	christmas_tree_id integer REFERENCES christmas_tree(id) ON DELETE CASCADE	
)

