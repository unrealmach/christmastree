-- Procedimiento para setear a 0 todos los nodos segun una fecha en especifico
DROP  FUNCTION IF EXISTS set_cero_all_points() CASCADE;
CREATE OR REPLACE FUNCTION set_cero_all_points(OUT is_valid boolean) 
RETURNS boolean
	AS $body$
DECLARE
		cron_text_date_day text ;
		cron_text_date_month text ;
		day_actual text;
		month_actual text;
		
BEGIN

is_valid:=FALSE;
SELECT to_char(now(), 'MM')  INTO month_actual;
SELECT to_char(now(), 'DD') INTO day_actual;

	SELECT value FROM parameters WHERE name = 'FECHA_REINICIO_PUNTOS_DIA' INTO cron_text_date_day;
	SELECT value FROM parameters WHERE name = 'FECHA_REINICIO_PUNTOS_MES' INTO cron_text_date_month;
	
	IF cron_text_date_day = day_actual AND cron_text_date_month = month_actual
		THEN
			UPDATE nodo SET points =0;
			is_valid:=TRUE;
	
	END IF;
	

END $body$ LANGUAGE 'plpgsql';

