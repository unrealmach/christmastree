import { AuthenticationService } from './../../../../service/auth.service';
import { User } from './../../../../models/user.model';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'm-profile',
	templateUrl: './profile.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent implements OnInit {

  userModel : User;

  constructor( private authenticationService:AuthenticationService,) {
    
    
   }

  ngOnInit() {
    console.log("profile");
     
    this.authenticationService.isUserData().subscribe(
      res=>{
        console.log(res);
        
      }
    );
    console.log(

    );
  }

}
