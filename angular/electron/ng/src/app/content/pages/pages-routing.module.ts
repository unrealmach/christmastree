import { AuthGuardService } from './../../guards/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';



const routes: Routes = [
	{
		//  path: '', redirectTo: '/dashboard', pathMatch: 'full' ,
		path: '',
		component: PagesComponent,
		// canActivate: [AuthGuardService],
		// Remove comment to enable login

		children: [
			/* ------- START  OWN --------- */
			{
				path: '',
				loadChildren: './components/dashboard/dashboard.module#DashboardModule',
				// canActivate: [AuthGuardService],
			},
			{
				path: 'clients',
				loadChildren: './components/cliente/cliente.module#ClienteModule',
				// canActivate: [AuthGuardService],
			},
			{
				path: 'cuenta',
				loadChildren: './components/cuenta/cuenta.module#CuentaModule',
				// canActivate: [AuthGuardService],
			},
		]
	},

	// {
	// 	path: 'login',
	// 	// canActivate: [NgxPermissionsGuard],
	// 	loadChildren: './auth/auth.module#AuthModule',
	// 	// data: {
	// 	// 	data: {
	// 	// 		expectedRole: 'ALL',
	// 	// 	}
	// 	// },
	// },
	{
		path: '404',
		component: ErrorPageComponent
	},
	{
		path: 'error/:type',
		component: ErrorPageComponent
	},

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
