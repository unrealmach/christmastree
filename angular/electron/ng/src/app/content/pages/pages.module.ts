
import { AgmCoreModule } from '@agm/core';
import { MatProgressBarModule, MatTreeModule, MatTableModule } from '@angular/material';
import { LayoutModule } from '../layout/layout.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { PartialsModule } from '../partials/partials.module';
import { ActionComponent } from './header/action/action.component';
import { ProfileComponent } from './header/profile/profile.component';
import { CoreModule } from '../../core/core.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { NO_ERRORS_SCHEMA } from '@angular/compiler/src/core';






@NgModule({
	declarations: [
		PagesComponent,
		ActionComponent,
		ProfileComponent,
		ErrorPageComponent,


		// ProgressBarConfigurableComponent,
		// ProgressbarComponent,

	],
	imports: [
		CommonModule,
		HttpClientModule,
		FormsModule,
		PagesRoutingModule,
		CoreModule,
		LayoutModule,
		PartialsModule,
		AngularEditorModule,
		MatProgressBarModule,
		AgmCoreModule,
		MatTableModule,
	],
	providers: [PagesComponent]
})
export class PagesModule {
}
