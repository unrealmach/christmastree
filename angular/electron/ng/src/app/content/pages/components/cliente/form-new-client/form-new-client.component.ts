import { ClientService } from './../../../../../service/crud/client.service';
import { AuthNoticeService } from './../../../../../core/auth/auth-notice.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { Component, OnInit, Output, Input, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { SpinnerButtonOptions } from '../../../../partials/content/general/spinner-button/button-options.interface';
import { TranslateService } from '@ngx-translate/core';
import * as objectPath from 'object-path';
import { INewClientDTORequest } from '../../../../../models/DTORequest/newClientDTO.model';

@Component({
  selector: 'm-form-new-client',
  templateUrl: './form-new-client.component.html',
  styleUrls: ['./form-new-client.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormNewClientComponent implements OnInit,OnDestroy {

  public model: INewClientDTORequest = { nombres: '', dni: '',celular:'',email:'',christmas_tree_id_in:1,code_ref_padre_in:'' };
	@Output() actionChange = new Subject<string>();
	public loading = false;

	@Input() action: string;

	@ViewChild('f') f: NgForm;
  errors: any = [];
  spinner: SpinnerButtonOptions = {
		active: false,
		spinnerSize: 18,
		raised: true,
		buttonColor: 'primary',
		spinnerColor: 'accent',
		fullWidth: false
	};

  
  constructor(
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private router: Router,
		public authNoticeService: AuthNoticeService,
		private clientService : ClientService
  ) { }

  ngOnInit(): void {
    
		if (!this.authNoticeService.onNoticeChanged$.getValue()) {
			const initialNotice = ``;
			this.authNoticeService.setNotice(initialNotice, 'success');
		}
	}

	ngOnDestroy(): void {
		this.authNoticeService.setNotice(null);
	}


  submit() {
		this.spinner.active = true;
		let notices = [];
		this.errors=[];
		if (this.validate(this.f)) {
			
			this.clientService.addNewClient(this.model)
			.subscribe(
				response=>{
					console.log(response);
					
					if(response.code==0){
						notices.push(this.translate.instant('GENERAL.FORMS.RESPONSES.VALID'));
						notices.push("¡ATENCIÓN! Su código de referencia es: <h2>"+  response.unique_code+"</h2>");
						this.authNoticeService.setNotice(notices.join('<br/>'), 'success');
					//	this.router.navigate(['/']);
					}else{
						this.errors.push(this.translate.instant('GENERAL.FORMS.RESPONSES.INVALID'));
						this.errors.push(response.message);
						this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');

					}
				
          if (!this.cdr['destroyed']) {
            this.cdr.detectChanges();
        }
				}
			);

			this.spinner.active = false;
		
		} else {
			console.log(this.f);

    }
    
    

  }
  
  validate(f: NgForm) {
    if (f.form.status === 'VALID') {
      this.authNoticeService.setNotice(null);
      return true;
    }

    this.errors = [];
    		if (objectPath.get(f, 'form.controls.email.errors.email')) {
          this.errors.push(this.translate.instant('AUTH.VALIDATION.INVALID', {name: this.translate.instant('AUTH.INPUT.EMAIL')}));
        }
    
      
     if (objectPath.get(f, 'form.controls.nombres.errors.required')) {
       this.errors.push(this.translate.instant('GENERAL.FORMS.VALIDATION.REQUIRED', { name: this.translate.instant('GENERAL.FORMS.TYPE.NOMBRES') }));
     }

     if (objectPath.get(f, 'form.controls.email.errors.required')) {
      this.errors.push(this.translate.instant('GENERAL.FORMS.VALIDATION.REQUIRED', { name: this.translate.instant('GENERAL.FORMS.TYPE.EMAIL') }));
    }

    if (objectPath.get(f, 'form.controls.email.errors.type')) {
      this.errors.push(this.translate.instant('GENERAL.FORMS.VALIDATION.INVALID', { name: this.translate.instant('GENERAL.FORMS.TYPE.EMAIL') }));
    }

    if (objectPath.get(f, 'form.controls.dni.errors.required')) {
      this.errors.push(this.translate.instant('GENERAL.FORMS.VALIDATION.REQUIRED', { name: this.translate.instant('GENERAL.FORMS.TYPE.DNI') }));
    }

    if (objectPath.get(f, 'form.controls.celular.errors.required')) {
      this.errors.push(this.translate.instant('GENERAL.FORMS.VALIDATION.REQUIRED', { name: this.translate.instant('GENERAL.FORMS.TYPE.CELULAR') }));
    }


     if (this.errors.length > 0) {
       this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
       this.spinner.active = false;
       if (!this.cdr['destroyed']) {
        this.cdr.detectChanges();
    }
     }

    return false;
  }


}
