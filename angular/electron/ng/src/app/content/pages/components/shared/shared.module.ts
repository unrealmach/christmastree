import { UppercaseDirective } from './directives/uppercase.directive';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwnAuthNoticeComponent } from './auth-notice/auth-notice.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [OwnAuthNoticeComponent, UppercaseDirective],
    exports:[OwnAuthNoticeComponent,UppercaseDirective],

})
export class SharedModule { }
