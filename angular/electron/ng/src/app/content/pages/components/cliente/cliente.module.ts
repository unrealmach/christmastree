import { FormNewClientComponent } from './form-new-client/form-new-client.component';

import { FormComponent } from './mostrar-informacion/form/form.component';
import { ClienteComponent } from './cliente.component';
import { FormsModule } from '@angular/forms';
import { PartialsModule } from './../../../partials/partials.module';
import { LayoutModule } from './../../../layout/layout.module';
import { PortletModule } from './../../../partials/content/general/portlet/portlet.module';
import { MatTooltipModule, MatCardModule, MatDividerModule, MatProgressBarModule, MatButtonModule, MatToolbarModule, MatTableModule, MatIconModule, MatSortModule, MatLabel, MatFormFieldModule, MatInputModule, MatCheckboxModule, MatSelectModule, MatProgressSpinnerModule, MatPaginatorModule, MatTabsModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; import { MostrarInformacionComponent } from './mostrar-informacion/mostrar-informacion.component';
import { SharedModule } from '../shared/shared.module';
import { OwnDataTableListCliComponent } from './list-client-data-table/data-table.component';


@NgModule({
	imports: [
		CommonModule,
		MatTooltipModule,
		PortletModule,
		LayoutModule,
		PartialsModule,
		MatCardModule,

		MatDividerModule,
		MatProgressBarModule,
		MatButtonModule,
		MatToolbarModule,

		FormsModule,

		MatInputModule,
		MatFormFieldModule,
		MatCheckboxModule,

		MatSelectModule,
		MatTabsModule,
		SharedModule,
		/** datatable */
		MatTableModule,
		MatIconModule,
		MatSortModule,
		MatProgressSpinnerModule,
		MatPaginatorModule,

		RouterModule.forChild([
			{
				path: '',
				component: ClienteComponent,
				// canActivate: [AuthGuardService]
			}
		])

	],

	declarations: [ClienteComponent, MostrarInformacionComponent,
		 FormComponent,OwnDataTableListCliComponent, FormNewClientComponent]
})
export class ClienteModule { }
