import { FormDatatableService } from './../../../../../../service/cuenta/historial-cuenta/form-datatable.service';
import { AuthNoticeService } from './../../../../../../core/auth/auth-notice.service';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { Component, OnInit, Output, Input, ViewChild, ChangeDetectorRef } from '@angular/core';
import { SpinnerButtonOptions } from '../../../../../partials/content/general/spinner-button/button-options.interface';
import { TranslateService } from '@ngx-translate/core';
import * as objectPath from 'object-path';

@Component({
  selector: 'm-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {


  public model: any = { unique_code: ''};
	

	@ViewChild('f') f: NgForm;
  errors: any = [];
  spinner: SpinnerButtonOptions = {
		active: false,
		spinnerSize: 18,
		raised: true,
		buttonColor: 'primary',
		spinnerColor: 'accent',
		fullWidth: false
	};

  

  constructor(
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    public authNoticeService: AuthNoticeService,
    private fdtService:FormDatatableService) { }

  ngOnInit() {
    
    
  }

  submit() {
		this.spinner.active = true;
		this.errors=[];
		if (this.validate(this.f)) {
      
      this.fdtService.setData({'unique_code_client':this.model});
			
			this.spinner.active = false;
		
		} else {
			console.log(this.f);

    }
    
    

  }

  validate(f: NgForm) {
    if (f.form.status === 'VALID') {
      this.authNoticeService.setNotice(null);
      return true;
    }

    this.errors = [];
    		if (objectPath.get(f, 'form.controls.unique_code.errors.required')) {
          this.errors.push(this.translate.instant('GENERAL.FORMS.VALIDATION.REQUIRED', {name: this.translate.instant('GENERAL.FORMS.TYPE.UNIQUECODECLIENT')}));
        }
    
     if (this.errors.length > 0) {
       this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
       this.spinner.active = false;
       if (!this.cdr['destroyed']) {
        this.cdr.detectChanges();
    }
     }

    return false;
  }

}
