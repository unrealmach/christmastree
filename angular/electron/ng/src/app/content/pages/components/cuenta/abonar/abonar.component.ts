import { AuthNoticeService } from './../../../../../core/auth/auth-notice.service';
import { Router } from '@angular/router';
import { CuentaService } from './../../../../../service/crud/cuenta.service';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { IAbonarArbolDTORequest } from './../../../../../models/DTORequest/abonarArbolDTO.model';
import { Component, OnInit, Output, Input, ViewChild, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { SpinnerButtonOptions } from '../../../../partials/content/general/spinner-button/button-options.interface';
import { TranslateService } from '@ngx-translate/core';
import * as objectPath from 'object-path';

@Component({
  selector: 'm-abonar',
  templateUrl: './abonar.component.html',
  styleUrls: ['./abonar.component.scss']
})
export class AbonarComponent implements OnInit,OnDestroy {


  public model: IAbonarArbolDTORequest = { cantidad: '', unique_code_client: '',abono:'',radiobtn:''};
	@Output() actionChange = new Subject<string>();
	public loading = false;

	@Input() action: string;

	@ViewChild('f') f: NgForm;
  errors: any = [];
  spinner: SpinnerButtonOptions = {
		active: false,
		spinnerSize: 18,
		raised: true,
		buttonColor: 'primary',
		spinnerColor: 'accent',
		fullWidth: false
  };
  
  
  


  constructor( private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private router: Router,
		public authNoticeService: AuthNoticeService,
    private cuentaService:CuentaService
    ) { }

  
    ngOnInit(): void {
    
      if (!this.authNoticeService.onNoticeChanged$.getValue()) {
        const initialNotice = ``;
        this.authNoticeService.setNotice(initialNotice, 'success');
      }
    }
  
    ngOnDestroy(): void {
      this.authNoticeService.setNotice(null);
    }

    submit() {
      this.spinner.active = true;
      let notices = [];
      this.errors=[];
      if (this.validate(this.f)) {
        
        this.cuentaService.abonarArbol(this.model)
        .subscribe(
          response=>{

            if(response.code==0){
              notices.push(this.translate.instant('GENERAL.FORMS.RESPONSES.VALID'));
              this.authNoticeService.setNotice(notices.join('<br/>'), 'success');
            //	this.router.navigate(['/']);
            }else{
              this.errors.push(this.translate.instant('GENERAL.FORMS.RESPONSES.INVALID'));
              this.errors.push(response.message);
              this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
  
            }
          
            if (!this.cdr['destroyed']) {
              this.cdr.detectChanges();
          }
          }
        );
  
        this.spinner.active = false;
      
      } else {
        console.log(this.f);
  
      }
      
      
  
    }
    
    validate(f: NgForm) {
      if (f.form.status === 'VALID') {
        this.authNoticeService.setNotice(null);
        return true;
      }
  
      this.errors = [];
          if (objectPath.get(f, 'form.controls.cantidad.errors.required')) {
            this.errors.push(this.translate.instant('GENERAL.FORMS.VALIDATION.REQUIRED', {name: this.translate.instant('GENERAL.FORMS.TYPE.CANTIDAD')}));
          }
      
        
       if (objectPath.get(f, 'form.controls.unique_code_client.errors.required')) {
         this.errors.push(this.translate.instant('GENERAL.FORMS.VALIDATION.REQUIRED', { name: this.translate.instant('GENERAL.FORMS.TYPE.UNIQUECODECLIENT') }));
       }
  
       if (objectPath.get(f, 'form.controls.radiobtn.errors.required')) {
        this.errors.push(this.translate.instant('GENERAL.FORMS.VALIDATION.REQUIRED', { name: this.translate.instant('GENERAL.FORMS.TYPE.ABONO') }));
      }
  
  
       if (this.errors.length > 0) {
         this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
         this.spinner.active = false;
         if (!this.cdr['destroyed']) {
          this.cdr.detectChanges();
      }
       }
  
      return false;
    }

    changeState() {
      this.model.abono = Math.round((Number(this.model.cantidad) * Number(this.model.radiobtn))*100)/100  + "";
    }

    
}
