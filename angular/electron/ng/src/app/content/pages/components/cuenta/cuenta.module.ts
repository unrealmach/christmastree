import { AbonarComponent } from './abonar/abonar.component';
import { CuentaComponent } from './cuenta.component';
import { FormsModule } from '@angular/forms';
import { PartialsModule } from './../../../partials/partials.module';
import { LayoutModule } from './../../../layout/layout.module';
import { PortletModule } from './../../../partials/content/general/portlet/portlet.module';
import { MatTooltipModule, MatCardModule, MatDividerModule, MatProgressBarModule, MatButtonModule, MatToolbarModule, MatTableModule, MatIconModule, MatSortModule, MatLabel, MatFormFieldModule, MatInputModule, MatCheckboxModule, MatSelectModule, MatProgressSpinnerModule, MatPaginatorModule, MatTabsModule, MatRadioModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { HistorialCuentaComponent } from './historial-cuenta/historial-cuenta.component';
import { OwnDataTableHistorialCuentaComponent } from './historial-cuenta/data-table/data-table.component';
import { FormComponent } from './historial-cuenta/form/form.component';
import { RetirarDineroComponent } from './retirar-dinero/retirar-dinero.component';
import { RetiroDineroFormComponent } from './retirar-dinero/form/form.component';
import { DarRegaloComponent } from './dar-regalo/dar-regalo.component';
import { DarRegaloFormComponent } from './dar-regalo/form/form.component';
@NgModule({
	imports: [
		CommonModule,
		MatTooltipModule,
		PortletModule,
		LayoutModule,
		PartialsModule,
		MatCardModule,

		MatDividerModule,
		MatProgressBarModule,
		MatButtonModule,
		MatToolbarModule,

		FormsModule,

		MatInputModule,
		MatFormFieldModule,
		MatCheckboxModule,

		MatSelectModule,
		MatTabsModule,
		SharedModule,
		/** datatable */
		MatTableModule,
		MatIconModule,
		MatSortModule,
		MatProgressSpinnerModule,
		MatPaginatorModule,
		MatRadioModule,



		/** ENd datatable */
		RouterModule.forChild([
			{
				path: '',
				component: CuentaComponent,
				// canActivate: [AuthGuardService]
			},
		])

	],
	declarations: [CuentaComponent,
		HistorialCuentaComponent,
		OwnDataTableHistorialCuentaComponent,
		FormComponent, RetirarDineroComponent,
		RetiroDineroFormComponent,
		DarRegaloFormComponent, 
		 AbonarComponent, DarRegaloComponent]
})
export class CuentaModule { }
