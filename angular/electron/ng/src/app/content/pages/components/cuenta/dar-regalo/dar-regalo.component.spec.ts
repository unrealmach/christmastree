import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DarRegaloComponent } from './dar-regalo.component';

describe('DarRegaloComponent', () => {
  let component: DarRegaloComponent;
  let fixture: ComponentFixture<DarRegaloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DarRegaloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DarRegaloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
