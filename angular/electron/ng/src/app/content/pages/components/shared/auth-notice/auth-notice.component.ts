import { AuthNotice } from './../../../../../core/auth/auth-notice.interface';
import { AuthNoticeService } from './../../../../../core/auth/auth-notice.service';
import { Component, ChangeDetectionStrategy, OnInit, Output, ChangeDetectorRef } from '@angular/core';


@Component({
	selector: 'm-own-auth-notice',
	templateUrl: './auth-notice.component.html',
	styleUrls: ['./auth-notice.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class OwnAuthNoticeComponent implements OnInit {
	@Output() type: any;
	@Output() message: any = '';

	constructor(public authNoticeService: AuthNoticeService
		,
		private cdr: ChangeDetectorRef,) {}

	ngOnInit() {
			this.authNoticeService.onNoticeChanged$.subscribe(
				(notice: AuthNotice) => {
					if(notice){
						this.message = notice.message;
						this.type = notice.type;
						if (!this.cdr['destroyed']) {
							this.cdr.detectChanges();
						}
					}
					
				}
			);
	}
}
