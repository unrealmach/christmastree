import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';
import { Angular2CsvModule } from 'angular2-csv';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { MaterialPreviewModule } from './../../../partials/content/general/material-preview/material-preivew.module';
import { PortletModule } from './../../../partials/content/general/portlet/portlet.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layout/layout.module';
import { PartialsModule } from '../../../partials/partials.module';
import { MatCardModule, MatDividerModule, MatTableModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatIconModule, MatButtonModule, MatProgressBarModule, MatToolbarModule, MatTreeModule, MatDatepicker, MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatInputModule, MatAutocompleteModule, MatOptionModule, MatTooltipModule } from '@angular/material';


@NgModule({
	imports: [
		// BrowserAnimationsModule,
		MatTooltipModule,
		CommonModule,
		PortletModule,
		LayoutModule,
		PartialsModule,
		MatCardModule,

		MatDividerModule,
		 MatProgressBarModule,
		MatButtonModule,
		MatToolbarModule,

		/** datatable */
		MatTableModule,
		MatIconModule,
		MatSortModule,
		MatProgressSpinnerModule,
		MatPaginatorModule,
		
		

		/** ENd datatable */

		AgmCoreModule,
		MaterialPreviewModule,
		MatTreeModule,
		MatIconModule,
		MatProgressBarModule,
		MatNativeDateModule,
		MatInputModule,
		FormsModule,
		ReactiveFormsModule,
		MatAutocompleteModule,
		MatOptionModule,
		Angular2CsvModule,
		MatDatepickerModule,
		MatFormFieldModule,
		AgmSnazzyInfoWindowModule,
		
		RouterModule.forChild([
			{
				path: '',
				component: DashboardComponent,
				// canActivate: [AuthGuardService]
			}
		])

	],
	providers: [],
	declarations: [DashboardComponent,
		 
	]
})
export class DashboardModule {  }

