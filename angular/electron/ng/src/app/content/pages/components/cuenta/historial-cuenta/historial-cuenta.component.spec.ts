import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialCuentaComponent } from './historial-cuenta.component';

describe('HistorialCuentaComponent', () => {
  let component: HistorialCuentaComponent;
  let fixture: ComponentFixture<HistorialCuentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialCuentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
