import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'm-mostrar-informacion',
  templateUrl: './mostrar-informacion.component.html',
  styleUrls: ['./mostrar-informacion.component.scss']
})
export class MostrarInformacionComponent implements OnInit {

  public modelResponse :Array<any>;
  constructor() {
    this.clearArray();
   }

   clearArray(){
    this.modelResponse= new Array<any>();
    this.modelResponse.push({cantidad_ultima_transaccion:'',celular:'',dni:'',email:'',monto:'',nombres:'',tipo:'',unique_code:''});
   }

  ngOnInit() {
  }

  public onBrake(response:any) {
     console.log(response);
     
    if(response && response.length>0){
      this.modelResponse = response;
    }else{
      this.clearArray();
    }
    
    
    
  }

}
