import { FormDatatableService } from './../../../../../../service/cuenta/historial-cuenta/form-datatable.service';
import { CuentaService } from './../../../../../../service/crud/cuenta.service';
import { QueryParamsModel } from './../../../../../../core/models/query-params.model';
import { ClientService } from './../../../../../../service/crud/client.service';
import { SnackBarService } from './../../../../../../service/snackbar.service';
import { Router, ActivatedRoute } from '@angular/router';
import {
	Component,
	OnInit,
	ElementRef,
	ViewChild,
	ChangeDetectionStrategy,
	Input,
	Output,
	ChangeDetectorRef
} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
// RXJS
import { debounceTime, distinctUntilChanged, tap, map } from 'rxjs/operators';
import { merge, forkJoin, Subject } from 'rxjs';

// Models


import { DataTableDataSource } from './data-table.data-source';

// Services


@Component({
	selector: 'own-m-data-table-historial-cuenta',
	templateUrl: './data-table.component.html',
	styleUrls: ['./data-table.component.scss']
})
export class OwnDataTableHistorialCuentaComponent implements OnInit {

	dataSource: DataTableDataSource;
	allowsDelete: boolean=false;
	@Input() action: string;

	//  public searchData :IVehicle ={internalId:'',date:'',alias:''};


	@Output() actionChange = new Subject<string>();
	displayedColumns = ['tipo','cantidad', 'montoAnterior', 'montoActual'];

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	selection = new SelectionModel<any>(true, []);

	options: any;


	data: any;


	constructor(
		private cdr: ChangeDetectorRef,
		private router: Router,
		private route: ActivatedRoute,
		private cuentaService:CuentaService,

		private snackBarService:SnackBarService,
		private fdtService:FormDatatableService,
	) { }



	ngOnInit() {


		//subscripción al evento del servicio de comunicacion	
		this.fdtService.readyForm.subscribe(isOpen => {
			
			
			this.loadItems();
			// this.searchData = this.filtersSearchClientsService.getSearchData();

		});

		// If the user changes the sort order, reset back to the first page.
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/

		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.loadItems();
				})
			)
			.subscribe();

		


		// Init DataSource
		this.dataSource = new DataTableDataSource(this.cuentaService);

		// First load
	//	this.loadItems(true);
	}



	loadItems(firstLoad: boolean = false) {
		const queryParams = new QueryParamsModel(
			{},
			'desc',
			
			this.sort.active='id',
			this.paginator.pageIndex,
			firstLoad ? 6 : this.paginator.pageSize
		);
		
		this.dataSource.loadItems(queryParams,this.fdtService.getSearchData());
		this.selection.clear();
		
		if (!this.cdr['destroyed']) {
			this.cdr.detectChanges();
		}

	}


	

}
