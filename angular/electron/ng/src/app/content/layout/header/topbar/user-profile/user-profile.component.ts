import { User } from './../../../../../models/user.model';
import { UserOperatorService } from './../../../../../service/user.service';
import { AuthenticationService } from './../../../../../service/auth.service';

import { ChangeDetectionStrategy, Component, ElementRef, HostBinding, Input, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

@Component({
	selector: 'm-user-profile',
	templateUrl: './user-profile.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserProfileComponent implements OnInit {
	@HostBinding('class')
	// tslint:disable-next-line:max-line-length
	classes = 'm-nav__item m-topbar__user-profile m-topbar__user-profile--img m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light';

	@HostBinding('attr.m-dropdown-toggle') attrDropdownToggle = 'click';

	@Input() avatar: string = './assets/app/media/img/users/iconuser.png';
	@Input() avatarBg: SafeStyle = '';

	@ViewChild('mProfileDropdown') mProfileDropdown: ElementRef;

	user: User;
	loading:boolean;

	constructor(
		private router: Router,
		private authService: AuthenticationService,
		private sanitizer: DomSanitizer,
		private userOperatorService: UserOperatorService,
		private cdr: ChangeDetectorRef,
	) {
		this.user = new User();
		this.loading=true;

	}

	ngOnInit(): void {
		if (!this.avatarBg)
			this.avatarBg = this.sanitizer.bypassSecurityTrustStyle('url(./assets/app/media/img/misc/user_profile_bg.jpg)');


		this.authService.isUserData().subscribe(
			res => console.log(res)

		)

		this.userOperatorService.getUserSubject().subscribe(
			res => console.log(res)

		)


		setTimeout(() => {
			this.getUserLocalStorage();
		}, 1000);
			
		




	}

	private getUserLocalStorage() {
		this.user = User.fromJSON(JSON.parse(localStorage.getItem('userInfo')));
		this.cdr.detectChanges();
		this.loading=false;
		console.log(this.user);

	}

	public logout() {
		this.authService.logout();
		//TODO: se refresca la pagina para vaciar los providers
		location.reload();
		setTimeout(() => {
			this.router.navigate(['/login']);	
		}, 500);
	}
}
