import { FormDatatableService } from './service/cuenta/historial-cuenta/form-datatable.service';

import { CuentaService } from './service/crud/cuenta.service';
import { ClientService } from './service/crud/client.service';
import { environment } from './../environments/environment';

import { Http, HttpModule } from '@angular/http';
import { CRUDService } from './service/crud.service';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AuthenticationModule } from './core/auth/authentication.module';
import { NgxPermissionsModule } from 'ngx-permissions';

import { LayoutModule } from './content/layout/layout.module';
import { PartialsModule } from './content/partials/partials.module';
import { CoreModule } from './core/core.module';
import { AclService } from './core/services/acl.service';
import { LayoutConfigService } from './core/services/layout-config.service';
import { MenuConfigService } from './core/services/menu-config.service';
import { PageConfigService } from './core/services/page-config.service';
// import { UserService } from './core/services/user.service';
import { UtilsService } from './core/services/utils.service';
import { ClassInitService } from './core/services/class-init.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GestureConfig, MatProgressSpinnerModule, MatSnackBarModule, MatDialogModule, MatTreeModule, MatIconModule, MatProgressBarModule } from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';

import { MessengerService } from './core/services/messenger.service';
import { ClipboardService } from './core/services/clipboard.sevice';

import { PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { LayoutConfigStorageService } from './core/services/layout-config-storage.service';
import { LogsService } from './core/services/logs.service';
import { QuickSearchService } from './core/services/quick-search.service';
import { SubheaderService } from './core/services/layout/subheader.service';
import { HeaderService } from './core/services/layout/header.service';
import { MenuHorizontalService } from './core/services/layout/menu-horizontal.service';
import { MenuAsideService } from './core/services/layout/menu-aside.service';
import { LayoutRefService } from './core/services/layout/layout-ref.service';
import { SplashScreenService } from './core/services/splash-screen.service';
import { DataTableService } from './core/services/datatable.service';

import { UserOperatorService } from './service/user.service';
import { AuthenticationService } from './service/auth.service';
import { AuthGuardService } from './guards/auth-guard.service';


import 'hammerjs';
import { ObservableService } from './service/observable.service';
import { SnackBarService } from './service/snackbar.service';


import { AgmCoreModule } from '@agm/core';
import { UtilService } from './service/util/util.service';

import { Angular2CsvModule } from 'angular2-csv';

import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';






const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	// suppressScrollX: true
};

@NgModule({
	declarations: [AppComponent ],
	

	imports: [
		BrowserAnimationsModule,
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		LayoutModule,
		PartialsModule,
		CoreModule,
		OverlayModule,
		AuthenticationModule,
		NgxPermissionsModule.forRoot(),
		NgbModule.forRoot(),
		TranslateModule.forRoot(),
		MatProgressSpinnerModule,
		//-----------start own -- //
		HttpModule,
		MatSnackBarModule,
		MatDialogModule,
		// Angular2CsvModule,

		AgmCoreModule.forRoot({
			apiKey: environment.agmkey
		  }),
		  AgmSnazzyInfoWindowModule,
		
		//----------- end own -- //
		
		
	],
	providers: [
		// ---------- start own ---------//
		CRUDService,
		ObservableService,
		
		SnackBarService,
		ClientService,
		UtilService,
		CuentaService,
		FormDatatableService,
		//---------- end own --------//
		AuthGuardService, UserOperatorService, AuthenticationService,

		AclService,
		LayoutConfigService,
		LayoutConfigStorageService,
		LayoutRefService,
		MenuConfigService,
		PageConfigService,
		UtilsService,
		ClassInitService,
		MessengerService,
		ClipboardService,
		LogsService,
		QuickSearchService,
		DataTableService,
		SplashScreenService,
		{
			provide: PERFECT_SCROLLBAR_CONFIG,
			useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		},

		// template services
		SubheaderService,
		HeaderService,
		MenuHorizontalService,
		MenuAsideService,
		{
			provide: HAMMER_GESTURE_CONFIG,
			useClass: GestureConfig
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
