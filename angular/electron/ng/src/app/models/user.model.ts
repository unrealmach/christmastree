export class User{

  login: string;
  name: string;
  email: string;
  photo: string;
  role: string;
  roles: string[];
  
  public static fromJSON(json: any): User {
        const object = Object.create(User.prototype);
        Object.assign(object, json);
        return object;
    }

}

export interface IUser{

    login: string;
    name: string;
    email: string;
    photo: string;
    role: string;
    roles: string[];
    

  
  }
