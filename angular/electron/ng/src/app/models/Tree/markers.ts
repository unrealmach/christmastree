
export interface marker {
	lat: number;
	lng: number;
  iconUrl?: string;
  info?:string;
}

export class marker {
	lat: number;
	lng: number;
  iconUrl?: string;
  info?:string;

  constructor(lat,lng,iconUrl=null,info=null){
    this.lat=lat;
    this.lng=lng;
    this.iconUrl=iconUrl
    this.info=info;
  }
}