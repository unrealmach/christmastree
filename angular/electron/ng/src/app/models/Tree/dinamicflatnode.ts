/** Flat node with expandable and level information */
export class DynamicFlatNode {
    
    constructor(public item: string, public level = 1, public expandable = false,
        public internalId:string=null,
      public isLoading = false) { }
  }
  