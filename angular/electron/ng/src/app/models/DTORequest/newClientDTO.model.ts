export class NewClientDTORequest{
    nombres: string;
    dni: string;
    celular:string;
    email:string;
    christmas_tree_id_in:number;
    code_ref_padre_in:string
  
    
    
    public static fromJSON(json: any): NewClientDTORequest {
          const object = Object.create(NewClientDTORequest.prototype);
          Object.assign(object, json);
          return object;
      }
  }
  
  export interface INewClientDTORequest{
    nombres?: string;
    dni?: string;
    celular?:string;
    email?:string;
    christmas_tree_id_in?:number;
    code_ref_padre_in?:string
   
    }
  