export class AbonarArbolDTORequest{
    cantidad: string;
    unique_code_client: string;
    abono :string;
    radiobtn : string;
    
    
    public static fromJSON(json: any): AbonarArbolDTORequest {
          const object = Object.create(AbonarArbolDTORequest.prototype);
          Object.assign(object, json);
          return object;
      }
  }
  
  export interface IAbonarArbolDTORequest{
    cantidad?: string;
    unique_code_client?: string;
    abono ?:string;
    radiobtn ?: string;
    }
  