import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserOperatorService } from '../service/user.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private userService: UserOperatorService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const expectedRole = route.data.expectedRole;
    const userInfo = JSON.parse(localStorage.getItem('userInfo'));
    const currentUser = localStorage.getItem('currentUser');
    if (expectedRole && expectedRole!=null && expectedRole != undefined) {
      if (currentUser && expectedRole === userInfo.roles[0]) {
        return true;
      } else {
        this.forceOut();
      }
    }

    if (currentUser) {
      return true;
    }


    // not logged in so redirect to login page with the return url
    this.forceOut();
  }

  forceOut() {
    // localStorage.setItem('currentUser','');
    // localStorage.setItem('userInfo','');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('userInfo');
    this.router.navigate(['login']);
    return false;
  }


}
