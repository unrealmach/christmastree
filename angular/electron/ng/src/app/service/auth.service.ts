import { catchError } from 'rxjs/operators';
import { CRUDService } from './crud.service';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { BaseService } from './base.service';
import { UserOperatorService } from './user.service';


@Injectable()
export class AuthenticationService extends BaseService {

    private logger = new Subject<boolean>();
    private loggedIn = false;
    private userData = new Subject<string>();

    constructor(private http: Http,
        private userService: UserOperatorService,
        private crudService: CRUDService) {
        super();
    }

    isUserData(): Observable<string> {
        return this.userData.asObservable();
    }

    isLoggedIn(): Observable<boolean> {
        return this.logger.asObservable();
    }

    login(model) {

        return this.userService.doLogin(model)
            .map(response => {

                const token = response.headers.get('Authorization');
                
                if (token) {
                    const user = { username: model.username, token: token };
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.loggedIn = true;

                    this.crudService.getUserInfo('/api/users/getUser/',model.username)
                    .subscribe(userInfo => {
                        
                        localStorage.setItem('userInfo', JSON.stringify(userInfo));
                        this.userData.next(JSON.stringify(userInfo));

                        this.userData.subscribe(
                            (res) => console.log(res),
                            err=>console.log(err)
                            
                        )
                        
                        
                    });
                    this.logger.next(this.loggedIn);
                }
                return response;

            }
            ).catch((error: any) => {
                 return Observable.throw(error.json().error || 'Server error') });


    }
    

    logout() {
        this.loggedIn = false;
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('userInfo');
        this.logger.next(this.loggedIn);
    }

    public getUserInfo(){
        return JSON.parse(localStorage.getItem('userInfo'));
    }

    public getRolesOfUser(){
        return this.getUserInfo().roles;
    }
}
