import { Injectable } from '@angular/core';


@Injectable()
export class UtilService {

    constructor() { }

    public getToday() {
        return this.today().day + "/" + this.today().month + "/" + this.today().year;
    }

    public getDate(date: myDate) {
        return date.day + "/" + date.month + "/" + date.year;
    }

    /**
     * yyyy-MM-dd
     */
    public getTodayHtml5() {
        return this.today().year + '-' + this.today().month + "-" + this.today().day;
    }

    public today(): iMyDate {
        var currentDate = new Date();
        var day = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        return new myDate(day, month, year);
    }

    public changeFormatDate(date: string) {
        let splitString: string[] = [];
        splitString = date.split("-");
        if (splitString.length > 0) {
            var day = splitString[2];
            var month = splitString[1];
            var year = splitString[0];
            return this.getDate(new myDate(day, month, year));
        }
        return date;
    }
}


class myDate {
    day: string;
    month: string;
    year: string;

    constructor(day, month, year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
}

interface iMyDate {
    day: string;
    month: string;
    year: string;
}