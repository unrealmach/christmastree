import { INewClientDTORequest } from './../../models/DTORequest/newClientDTO.model';
import { AuthenticationService } from './../auth.service';
import { ObservableService } from '../observable.service';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';



@Injectable()
export class ClientService {
    _base = "/api/client";
   
    _rutaGetInfoClient =this._base+"/getInfoClient";
    _rutaAddNewClient =this._base+"/addNewClient";
    _rutaGetAllClientst =this._base+"/getAllClients";

    
    constructor(private service: ObservableService,
        
        private authenticationService: AuthenticationService) {

    }

    public getInfoClient(unique_code_client: string): Observable<any> {
        return this.service.getUrlServicioGet(this._rutaGetInfoClient+"/"+unique_code_client);
    }

    public addNewClient(iNewClientDTORequest : INewClientDTORequest): Observable<any> {
        return this.service.getUrlServicioPost(this._rutaAddNewClient,
            iNewClientDTORequest
        );
    }

    public getAllClients(): Observable<any> {
        return this.service.getUrlServicioGet(this._rutaGetAllClientst);
    }

    
    
   
}