import { IAbonarArbolDTORequest } from './../../models/DTORequest/abonarArbolDTO.model';
import { AuthenticationService } from '../auth.service';
import { ObservableService } from '../observable.service';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';



@Injectable()
export class CuentaService {
    _base = "/api/cuenta";
   
    _rutaAbonarArbol =this._base+"/abonarArbol";
    _rutaGetMovimientosCliente =this._base+"/getMovimientosCliente";
    _rutaWithdrawals=this._base+"/withdrawals";

    _rutaDoGift=this._base+"/doGift";
    
    constructor(private service: ObservableService,
        
        private authenticationService: AuthenticationService) {

    }


    public abonarArbol(iAbonarArbolDTORequest : IAbonarArbolDTORequest): Observable<any> {
        return this.service.getUrlServicioPost(this._rutaAbonarArbol,
            iAbonarArbolDTORequest
        );
    }

    public getMovimientosCliente(unique_code_client: string): Observable<any> {
        return this.service.getUrlServicioGet(this._rutaGetMovimientosCliente+"/"+unique_code_client);
    }

    public withdrawals(unique_code_client: string,cantidad:string): Observable<any> {
        return this.service.getUrlServicioPost(this._rutaWithdrawals,{
            cantidad:cantidad,unique_code_client:unique_code_client
        });
    }
    
    public doGift(unique_code_client: string): Observable<any> {
        return this.service.getUrlServicioGet(this._rutaDoGift+"/"+unique_code_client);
    }

    
    
   
}