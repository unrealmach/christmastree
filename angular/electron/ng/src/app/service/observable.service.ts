import { Observable } from 'rxjs';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { BaseService } from './base.service';

@Injectable()
export class ObservableService extends BaseService {

    private headers = new Headers();
    private requestOptions = new RequestOptions();


    constructor(private http: Http) {
        super();
        // this.headers.append('Access-Control-Allow-Origin', '*');
        this.headers.append('Content-Type', 'application/json');
      //   this.headers.append('Authorization', "41ce23b6-a077-4e86-b120-acb2931a9615");
        this.requestOptions = new RequestOptions({
            headers: this.headers
        });
    }

    // private refreshJwt() {
    //     this.headers = new Headers();
    //     this.headers.append('Content-Type', 'application/json');
    //     this.headers.append('Authorization', this.jwt().headers.get('authorization'));
    //     this.requestOptions = new RequestOptions({
    //         headers: this.headers
    //     });
    // }

    public getUrlServicioGet(servicio: string) {
        // this.refreshJwt();
        return this.http.get(super.getUrl() + servicio, this.requestOptions)
            .map(response => {
                return response.json();
            })
            .catch(err => { return Observable.throw(err || 'Error desconocidon la respuesta del servidor.') });
    }

    public getUrlServicioPost(servicio: string, objeto: any) {
        // this.refreshJwt();
        return this.http.post(super.getUrl() + servicio,
            JSON.stringify(objeto),
            this.requestOptions)
            .map(response => {
                return this.extractData(response);
            })

            .catch(err => { return Observable.throw(err || 'Error desconocidon la respuesta del servidor.') });
    }





    private extractData(res) {
        return res.text() ? res.json() : {};
    }

    // public getUrlServicioPut(servicio: string, objeto: any) {
    //     return this.http.put(servicio,
    //         JSON.stringify(objeto),
    //         this.requestOptions)
    //         .map(response => {
    //             return response.json();
    //         })
    //     //  .catch(err => this.alerta.mostrarAlertaErrorObservable(err));
    // }

    // public getUrlServicioDelete(servicio: string, objeto?: any) {
    //     return this.http.put(servicio,
    //         JSON.stringify(objeto), { headers: this.headers, withCredentials: true })
    //         .map(response => {
    //             return response.json();
    //         })
    //     //    .catch(err => this.alerta.mostrarAlertaErrorObservable(err));
    // }
    // public getUrlServicioRemover(servicio: string) {
    //     return this.http.delete(servicio, { withCredentials: true })
    //         .map(resul => {
    //             return null;
    //         })
    //     //      .catch(err => this.alerta.mostrarAlertaErrorObservable(err));
    // }
}