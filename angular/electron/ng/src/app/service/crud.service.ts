import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { ObservableService } from './observable.service';
import { BaseService } from './base.service';

@Injectable()
export class CRUDService   {

    constructor(private service: ObservableService) {
        
    }

    public login(ruta: any, data: any): Observable<any> {
        return this.service.getUrlServicioPost( ruta, data);
    }

    public getEvents(ruta: any): Observable<any> {
        return this.service.getUrlServicioGet(ruta);
    }

    public getVehiclesCD(ruta:any): Observable<any> {
        return this.service.getUrlServicioGet(ruta);
        
    }
//------------------------------------------------------
    public getUserInfo(ruta:any,login:any): Observable<any> {
        return this.service.getUrlServicioGet(ruta+login);
    }

    public createOrEditUser(ruta: any, data: any): Observable<any> {
        return this.service.getUrlServicioPost( ruta, data);
    }

    public deleteUser(ruta:any,login:string): Observable<any> {
        return this.service.getUrlServicioGet( ruta +"/"+login);
    }


    // public editUser(ruta:any,data:any): Observable<any>{
    //     return this.service.getUrlServicioPost( ruta, data);
    // }

    public resetPassword(ruta:any,login:any){
        return this.service.getUrlServicioGet(ruta+login);
    }

    public changePasswordUser(ruta:any,username:any,oldPass:any,newPass:any): Observable<any>{
        return this.service.getUrlServicioPost(ruta,{ login: username, oldPassword: oldPass, newPassword: newPass });
    }

    public getAllRoles(ruta:any){
        return this.service.getUrlServicioGet(ruta);
    }



}