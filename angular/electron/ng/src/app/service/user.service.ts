import { CRUDService } from './crud.service';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import { BaseService } from './base.service';
import { User } from '../models/user.model';
import 'rxjs/Rx';

@Injectable()
export class UserOperatorService extends BaseService {

    private userSubject = new Subject<any>();
    private user: User;

    constructor(private http: Http, private crudService: CRUDService) {
        super();
        this.userSubject = new Subject();
    }


    public getUserSubject(): Observable<User> {
        return this.userSubject.asObservable();
    }

    public setUserSubject(response:Response){
        this.userSubject.next(User.fromJSON(response));
    }

    public doLogin(usuario) {


        const headers = new Headers();
        headers.append('Access-Control-Allow-Origin', '*');
        return this.http.post(
            super.getUrl() + "/login",
            JSON.stringify(usuario), { headers: headers, withCredentials: true })
            .map(response => {
                return response
            },
            )
            .catch((error: any) => { return Observable.throw(error || 'Server error') });
    }

    // getUser(login: string) {
    //     const headers = new Headers();
    //     headers.append('Access-Control-Allow-Origin', '*');
    //     const requestOptions = new RequestOptions({
    //         headers: headers
    //     });
    //     return this.http.get(super.getUrl() + '/api/users/getUser/' + login, this.jwt())
    //         .map(response => {


    //             return response.json();

    //         });
    // }


    // getUsers() {
    //     const headers = new Headers();
    //     headers.append('Access-Control-Allow-Origin', '*');
    //     const requestOptions = new RequestOptions({
    //         headers: headers
    //     });
    //     return this.http.get(super.getUrl() + '/api/users/getUsers', this.jwt())
    //         .map(response => {
    //             console.log(response.json());
                
    //             return response.json();

    //         });
    // }

    // resetUserPassword(login: string) {
    //     const headers = new Headers();
    //     headers.append('Access-Control-Allow-Origin', '*');
    //     const requestOptions = new RequestOptions({
    //         headers: headers
    //     });
    //     //TODO: check URL y JWT no tiene
    //     return this.http.get(super.getUrl() + '/api/user/resetPassword/' + login, this.jwt())
    //         .map(response => {
    //             console.log(response);
                
    //             return response.json();

    //         });
    //  }

    

    // getUserInfo(username: string) {
    //     const headers = new Headers();
    //     headers.append('Access-Control-Allow-Origin', '*');
    //     const requestOptions = new RequestOptions({
    //         headers: headers
    //     });
    //     return this.http.get(super.getUrl() + '/api/users/getUser/' + username, this.jwt())
    //         .map(response => {
    //             this.user = User.fromJSON(response.json());
    //             localStorage.setItem('userInfo', JSON.stringify(this.user));
    //             this.userSubject.next(this.user);
    //             return response.json();

    //         })
    //         ;
    // }

    // createUser(user: User) {



    //     // return this.http.post(super.getUrl() + '/api/users/createUser',
    //     //  JSON.stringify(user), requestOptions)
    //     //     .map(response => {

    //     //         return response.json();

    //     //     })
    //     //     .catch((error: any) => { return Observable.throw(error || 'Server error') });;

    // }

    // changePassword(username: string, oldPassword: string, newPassword: string) {
    //     const headers = this.jwt().headers;
    //     headers.append('Access-Control-Allow-Origin', '*');
    //     headers.append('Content-Type', 'application/json');
    //     //headers.append(this.jwt().headers); 
    //     const requestOptions = new RequestOptions({
    //         headers: headers
    //     });
    //     return this.http.post(super.getUrl() + '/api/users/changePassword',
    //         JSON.stringify({ login: username, oldPassword: oldPassword, newPassword: newPassword }), requestOptions)
    //         .map(response => {

    //             return response.json();

    //         });
    // }

    // editUser(user: User) {
    //     const headers = this.jwt().headers;
    //     headers.append('Access-Control-Allow-Origin', '*');
    //     headers.append('Content-Type', 'application/json');
    //     //headers.append(this.jwt().headers); 
    //     const requestOptions = new RequestOptions({
    //         headers: headers
    //     });
    //     return this.http.post(super.getUrl() + '/api/users/editUser', JSON.stringify(user), requestOptions)
    //         .map(response => {

    //             return response.json();

    //         });

    // }




    // getAllRoles() {
    //     const headers = new Headers();
    //     headers.append('Access-Control-Allow-Origin', '*');
    //     const requestOptions = new RequestOptions({
    //         headers: headers
    //     });
    //     return this.http.get(super.getUrl() + '/api/users/getAllRoles', this.jwt())
    //         .map(response => {

    //             return response.json();

    //         });
    // }

    // deleteUser(login: string) {
    //     const headers = new Headers();
    //     headers.append('Access-Control-Allow-Origin', '*');
    //     const requestOptions = new RequestOptions({
    //         headers: headers
    //     });
    //     return this.http.get(super.getUrl() + '/api/users/deleteUser/' + login, this.jwt())
    //         .map(response => {

    //             return response.json();

    //         });
    // }

    



}
