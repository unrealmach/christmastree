import { UtilService } from './../../util/util.service';


import { Injectable, Output, EventEmitter } from '@angular/core';



@Injectable()
export class FormDatatableService {

  private searchData: any = { unique_code_client:'' };


  private resapi: any;

  @Output() readyForm: EventEmitter<boolean> = new EventEmitter();


  constructor() {
    
  }




  /**
   * Permite modificar los datos a buscar en el sistema y
   * emite un evento para indicar que se han modificado los datos de busqueda
   
   */
  public setData(data: any) {
    this.searchData.unique_code_client = data.unique_code_client;
    this.emitEvent();
  }



  public emitEvent() {
    this.readyForm.emit(true);
  }


  public setResApi(res) {
    this.resapi = res;

  }

 
  public getSearchData() {
    return this.searchData;
  }




}


