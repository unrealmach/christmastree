import { AuthenticationService } from './../service/auth.service';
// tslint:disable-next-line:no-shadowed-variable
import { ConfigModel } from '../core/interfaces/config';

// tslint:disable-next-line:no-shadowed-variable
export class MenuConfig implements ConfigModel {
	public config: any = {};

	constructor( ) {
		//AQUI verdad :P
		// let rolUser = JSON.parse(localStorage.getItem("userInfo")).roles[0];
		let itemsMenu = new Array();

		itemsMenu.push({
			title: 'Dashboard',
			// desc: 'Dashboard',
			root: true,
			icon: 'flaticon-calendar-with-a-clock-time-tools',
			page: '/',
			// badge: {type: 'm-badge--danger', value: '2'},
			translate: 'MENU.DASHBOARD'
		});

		
		itemsMenu.push({
			title: 'Clientes',
			// desc: 'Registrar Nuevo Referido',
			root: true,
			icon: 'flaticon-network',
			page: '/clients',
			// badge: {type: 'm-badge--danger', value: '2'},
			// translate: 'MENU.DASHBOARD'
		},);
		itemsMenu.push({
			title: 'Cuenta',
			// desc: 'Aportar al Árbol',
			root: true,
			icon: 'flaticon-piggy-bank' ,
			page: '/cuenta',
			translate: 'MENU.DASHBOARD'
		});
		

		this.config = {
			header: {
				self: {},
				items: [
				
				]
			},
			aside: {
				self: {},
				items: itemsMenu
			}
		};
	}
}