// Spain
export const locale = {
	lang: 'es',
	data: {
		TRANSLATOR: {
			SELECT: 'Elige tu idioma',
		},
		MENU: {
			NEW: 'nuevo',
			ACTIONS: 'Comportamiento',
			CREATE_POST: 'Crear nueva publicación',
			REPORTS: 'Informes',
			APPS: 'Aplicaciones',
			DASHBOARD: 'Tablero'
		},
		GENERAL:{
			FORMS:{
				VALIDATION: {
					INVALID: '{{name}} no es válido',
					REQUIRED: '{{name}} es requerido',
					MIN_LENGTH: '{{name}} debe de tener {{min}} caracteres como mínimo',
					AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
					NOT_FOUND: 'La petición {{name}} no se encontro',
					INVALID_LOGIN: 'El detalle del login es incorrecto',
					
				},
				TYPE:{
					EMAIL:"Email",
					NOMBRES:"Nombres",
					CELULAR:"Celular",
					DNI:"D.N.I",
					CANTIDAD:"Cantidad",
					UNIQUECODECLIENT:"Código de referencia",
					ABONO:'Abono'
				},
				RESPONSES:{
					VALID :'Registro guardado con éxito',
					INVALID:'Error desde el servidor',
				}
			}
		},
		AUTH: {
			GENERAL: {
				OR: 'O',
				SUBMIT_BUTTON: 'Enviar',
				NO_ACCOUNT: 'No tienes una cuenta?',
				SIGNUP_BUTTON: 'Registrar',
				FORGOT_BUTTON: 'Se te olvidó tu contraseña',
				BACK_BUTTON: 'Espalda',
				PRIVACY: 'Intimidad',
				LEGAL: 'Legal',
				CONTACT: 'Contacto',
				REGISTER:'Registrar',
				UPDATE:'Actualizar'
			},
			LOGIN: {
				TITLE: 'Ingresar',
				BUTTON: 'Validar',
			},
			FORGOT: {
				TITLE: 'Contraseña olvidada?',
				DESC: 'Ingrese su correo electrónico para restablecer su contraseña',
			},
			REGISTER: {
				TITLE: 'Crear cuenta',
				DESC: 'Ingresa la información para registrar una cuenta',

				SUCCESS: {
					CREATE: 'Esta cuenta a sido creada exitosamente.',
					UPDATE: 'Esta cuenta a sido actualizada exitosamente.',
				}
			},
			INPUT: {
				EMAIL: 'Email',
				FULLNAME: 'Fullname',
				PASSWORD: 'Password',
				CONFIRM_PASSWORD: 'Confirm Password',
				NOMBRE: 'Nombre',
				LOGIN: 'Login',
				ROL: 'Rol',
				NAME:'Nombre',

			},
			VALIDATION: {
				INVALID: '{{name}} no es válido',
				REQUIRED: '{{name}} es requerido',
				MIN_LENGTH: '{{name}} debe de tener {{min}} caracteres como mínimo',
				AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
				NOT_FOUND: 'La petición {{name}} no se encontro',
				INVALID_LOGIN: 'El detalle del login es incorrecto'
			}
		},
		UPDATEPASSWORD:{
			OLDPASSORWD: "Anterior password",
			NEWPASSWORD: "Nuevo password",
			RENEWPASSWORD: "Repetir nuevo password",
			SINCOINCIDENCIA:"No coincide el campo de repetición de password"

		},

		ECOMMERCE: {
			COMMON: {
				SELECTED_RECORDS_COUNT: 'Selected records count: ',
				ALL: 'All',
				SUSPENDED: 'Suspended',
				ACTIVE: 'Active',
				FILTER: 'Filter',
				BY_STATUS: 'by Status',
				BY_TYPE: 'by Type',
				BUSINESS: 'Business',
				INDIVIDUAL: 'Individual',
				SEARCH: 'Search',
				IN_ALL_FIELDS: 'in all fields'
			},
			ECOMMERCE: 'eCommerce',
			CUSTOMERS: {
				CUSTOMERS: 'Customers',
				CUSTOMERS_LIST: 'Customers list',
				NEW_CUSTOMER: 'New Customer',
				DELETE_CUSTOMER_SIMPLE: {
					TITLE: 'Customer Delete',
					DESCRIPTION: 'Are you sure to permanently delete this customer?',
					WAIT_DESCRIPTION: 'Customer is deleting...',
					MESSAGE: 'Customer has been deleted'
				},
				DELETE_CUSTOMER_MULTY: {
					TITLE: 'Customers Delete',
					DESCRIPTION: 'Are you sure to permanently delete selected customers?',
					WAIT_DESCRIPTION: 'Customers are deleting...',
					MESSAGE: 'Selected customers have been deleted'
				},
				UPDATE_STATUS: {
					TITLE: 'Status has been updated for selected customers',
					MESSAGE: 'Selected customers status have successfully been updated'
				},
				EDIT: {
					UPDATE_MESSAGE: 'Customer has been updated',
					ADD_MESSAGE: 'Customer has been created'
				}
			}
		}
	}
};
