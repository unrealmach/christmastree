# ChristmasTree

##  Electron + Angular + Spring boot + Postgresql

1st version for the referred clients system by # ElMayorista store

## Electron + Angular

**Commands**

```javascript
# Install the dependencies of internal angular project
# Into of the folder /angular/electron/ng
npm install

# Run the electron project 
# Into of the folder /angular/electron
npm install
npm build-start
npm start
```

## Postgresql

**Step**

1- Is necesary have a empty database with name: **ChristmasTree**

## SpringBoot

**Steps**

```bash
# Install the dependencies of maven project
# Into of the root folder of project
mvn install 
	or download sources with any IDLE

# Run the project 
# Into of the root folder of project
# We need instance a new program, to this is necessary create a new package
mvn package

# Execute the program
# Into of the /target folder
java - jar christmasTree-XXXX.jar

```

To edit the source connection of SpringBoot find in /src/main/resources/application.properties file





